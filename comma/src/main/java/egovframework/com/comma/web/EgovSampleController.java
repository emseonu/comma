/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package egovframework.com.comma.web;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import egovframework.com.comma.service.EgovSampleService;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springmodules.validation.commons.DefaultBeanValidator;

/**
 * @Class Name : EgovSampleController.java
 * @Description : EgovSample Controller Class
 * @Modification Information
 * @ @ 수정일 수정자 수정내용 @ --------- --------- ------------------------------- @
 *   2009.03.16 최초생성
 *
 * @author 개발프레임웍크 실행환경 개발팀
 * @since 2009. 03.16
 * @version 1.0
 * @see
 *
 * 		Copyright (C) by MOPAS All right reserved.
 */

@Controller
public class EgovSampleController {

	/** EgovSampleService */

	@Resource(name = "sampleService")
	private EgovSampleService service;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	/*----------------------------------------페이지 이동----------------------------------------*/
	/* 조직도 관리 메뉴 */
	@RequestMapping(value = "/orgManage.do")
	public String orgManage(Model model) throws Exception {
		return "org/orgManage";
	}

	/* 프로젝트 관리 메뉴 */
	@RequestMapping(value = "/proManage.do")
	public String projectManage(Model model) throws Exception {
		return "project/proManage";
	}

	/* 구성원 관리 메뉴 */
	@RequestMapping(value = "/memManage.do")
	public String memberManage(Model model) throws Exception {
		return "member/memManage";
	}

	/* 프로젝트팀 관리 메뉴 */
	@RequestMapping(value = "/teamManage.do")
	public String teamManage(Model model) throws Exception {
		return "team/teamManage";
	}

	/*----------------------------------------AJAX----------------------------------------*/

	/* 프로젝트단위 jsondata */
	@RequestMapping(value = "/setProData.do", method = RequestMethod.POST)
	@ResponseBody
	public List<Map<String, Object>> setProData() throws Exception {
		List<Map<String, Object>> result = service.setProData();
		return result;
	}

	/* 팀구성원단위 jsondata */
	@RequestMapping(value = "/setTeamData.do", method = RequestMethod.POST)
	@ResponseBody
	public List<Map<String, Object>> setTeamData() throws Exception {
		List<Map<String, Object>> result = service.setTeamData();
		return result;
	}

	/* 추가될 project id */
	@RequestMapping(value = "/getProNextID.do", method = RequestMethod.POST)
	@ResponseBody
	public String getProNextID() throws Exception {
		String result = service.getProNextID();
		return result;
	}

	/* 추가될 dept id */
	@RequestMapping(value = "/getDeptNextID.do", method = RequestMethod.POST)
	@ResponseBody
	public String getDeptNextID() throws Exception {
		String result = service.getDeptNextID();
		System.out.println(result);
		return result;
	}

	/* 수정된 project sheet 저장 */
	@RequestMapping(value = "/saveProSheet.do", method = RequestMethod.POST)
	@ResponseBody
	public int saveProSheet(@RequestBody Map<String, List<Map<String, Object>>> proList) throws Exception {
		int s = proList.get("data").size();
		int count = 0;
		for (int i = 0; i < s; i++) {
			Object p = proList.get("data").get(i).get("sStatus");
			Map<String, Object> d = proList.get("data").get(i);

			if (p.equals("U")) {
				service.updateProSheet(d);
				count++;
			} else if (p.equals("I")) {
				service.insertProSheet(d);
				count++;
			} else if (p.equals("D")) {
				service.deleteProSheet(d);
				count++;
			}

		}
		return count;
	}

	/* 구성원단위 json */
	@RequestMapping(value = "/getMemData.do", method = RequestMethod.POST)
	@ResponseBody
	public List<Map<String, Object>> getMemData() throws Exception {
		List<Map<String, Object>> result = service.getMemData();
		return result;
	}

	/* 구성원 이미지 저장 */
	@RequestMapping(value = "/saveMemImg.do", method = RequestMethod.POST)
	@ResponseBody
	public String saveMemImg(MultipartHttpServletRequest multi) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		boolean success = false;
		String img = "";
		String path = "C:/upload/"; // 실제 저장경로
		String shortPath = "./upload/"; // 로드 경로

		// server.xml : <Context path="/comma/upload" reloadable="true"
		// docBase="C:/upload/"/>
		File dir = new File(path);

		if (!dir.isDirectory()) {
			dir.mkdirs();
		}

		Iterator<String> iter = multi.getFileNames();

		while (iter.hasNext()) {
			String uploadFileName = iter.next();
			UUID uuid = UUID.randomUUID();
			MultipartFile mFile = multi.getFile(uploadFileName);
			String oriFileName = mFile.getOriginalFilename();
			String saveFileName = uuid + "_" + oriFileName;
			if (new File(path + saveFileName).exists()) {
				saveFileName = saveFileName + "_" + System.currentTimeMillis();
			}

			try {
				img = shortPath + saveFileName;
				mFile.transferTo(new File(path + saveFileName));
				System.out.println(path + saveFileName);
				System.out.println(img);
				success = true;
			} catch (IllegalStateException e) {
				e.printStackTrace();
				success = false;

			} catch (IOException e) {
				e.printStackTrace();
				success = false;
			}

		}
		System.out.println(success);
			return img;


	}

	/* 추가될 member id */
	@RequestMapping(value = "/getMemNextID.do", method = RequestMethod.POST)
	@ResponseBody
	public String getMemNextID() throws Exception {
		String result = service.getMemNextID();
		return result;
	}

	/* 수정된 member sheet 저장 */
	@RequestMapping(value = "/saveMemSheet.do", method = RequestMethod.POST)
	@ResponseBody
	public int saveMemSheet(@RequestBody Map<String, List<Map<String, Object>>> memJson) throws Exception {

		int s = memJson.get("data").size();
		int count = 0;
		for (int i = 0; i < s; i++) {
			Object p = memJson.get("data").get(i).get("sStatus");
			Map<String, Object> d = memJson.get("data").get(i);
			if (p.equals("U")) {
				service.updateMemSheet(d);
				count++;
			} else if (p.equals("I")) {
				service.insertMemSheet(d);
				count++;
			} else if (p.equals("D")) {
				service.deleteMemSheet(d);
				count++;
			}
		}
		return count;
	}

	/* 프로젝트만 data */
	@RequestMapping(value = "/proListData.do", method = RequestMethod.POST)
	@ResponseBody
	public List<Map<String, Object>> proListData() throws Exception {
		List<Map<String, Object>> result = service.proListData();
		return result;
	}

	/* 팀원/부서만 data */
	@RequestMapping(value = "/getTeamData.do", method = RequestMethod.POST)
	@ResponseBody
	public List<Map<String, Object>> getTeamData() throws Exception {
		List<Map<String, Object>> result = service.getTeamData();
		return result;
	}

	/* 매니저만data */
	@RequestMapping(value = "/managerData.do", method = RequestMethod.POST)
	@ResponseBody
	public List<Map<String, Object>> managerData() throws Exception {
		List<Map<String, Object>> result = service.managerData();
		return result;
	}

	/* combo item 위한 부서data */
	@RequestMapping(value = "/deptCombo.do", method = RequestMethod.POST)
	@ResponseBody
	public List<Map<String, Object>> deptCombo(@RequestBody Map<String, String> map) throws Exception {
		List<Map<String, Object>> result = service.deptCombo(map.get("PRO_ID"));

		return result;
	}

	/* 수정된 team sheet 저장 */
	@RequestMapping(value = "/saveTeamSheet.do", method = RequestMethod.POST)
	@ResponseBody
	public int saveTeamSheet(@RequestBody Map<String, List<Map<String, Object>>> memJson) throws Exception {

		int s = memJson.get("data").size();
		int count = 0;
		for (int i = 0; i < s; i++) {
			Object p = memJson.get("data").get(i).get("sStatus");
			Map<String, Object> d = memJson.get("data").get(i);
			System.out.println(memJson.get("data").get(i));
			if (p.equals("U")) {
				service.updateTeamSheet(d);
				count++;
			} else if (p.equals("I")) {
				service.insertTeamSheet(d);
				count++;
			} else if (p.equals("D")) {
				service.deleteTeamSheet(d);
				count++;
			}
		}
		return count;
	}

	/* 수정된 team manager sheet 저장 */
	@RequestMapping(value = "/saveManSheet.do", method = RequestMethod.POST)
	@ResponseBody
	public int saveManSheet(@RequestBody Map<String, List<Map<String, Object>>> memJson) throws Exception {

		int s = memJson.get("data").size();
		int count = 0;
		for (int i = 0; i < s; i++) {
			System.out.println(memJson.get("data").get(i));
			Object p = memJson.get("data").get(i).get("sStatus");
			Map<String, Object> d = memJson.get("data").get(i);
			if (p.equals("I")) {
				service.deleteManSheet(d);
				service.insertManSheet(d);
				count++;
			} else if (p.equals("D")) {
				service.deleteManSheet(d);
				count++;
			}
		}
		return count;
	}
	/* 구성원팝업 : 프로젝트 정보  */
	@RequestMapping(value = "/getProNM.do", method = RequestMethod.POST)
	@ResponseBody
	public  List<Map<String, Object>> getProNM(@RequestBody Map<String, String> map) throws Exception {
		List<Map<String, Object>> result = service.getProNM(map.get("DEPT_ID"));

		return result;
	}

	/* 프로젝트팝업 : 차트정보  */
	@RequestMapping(value = "/getChartData.do", method = RequestMethod.POST)
	@ResponseBody
	public  List<Map<String, Object>> getChartData(@RequestBody Map<String, String> map) throws Exception {
		List<Map<String, Object>> result = service.getChartData(map.get("PRO_ID"));

		return result;
	}


}
