/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package egovframework.com.comma.service;

import java.util.Date;

public class ProjectVO {

	private String pro_dept_key;
	private String pro_id;
	private String pro_upid;
	private String pro_lvl;
	private String pro_nm;
	private Date pro_fromd;
	private Date pro_tod;
	private String pro_loca;
	private double pro_loca_x;
	private double pro_loca_y;

	public String getPro_dept_key() {
		return pro_dept_key;
	}

	public void setPro_dept_key(String pro_dept_key) {
		this.pro_dept_key = pro_dept_key;
	}

	public String getPro_id() {
		return pro_id;
	}

	public void setPro_id(String pro_id) {
		this.pro_id = pro_id;
	}

	public String getPro_upid() {
		return pro_upid;
	}

	public void setPro_upid(String pro_upid) {
		this.pro_upid = pro_upid;
	}

	public String getPro_lvl() {
		return pro_lvl;
	}

	public void setPro_lvl(String pro_lvl) {
		this.pro_lvl = pro_lvl;
	}

	public String getPro_nm() {
		return pro_nm;
	}

	public void setPro_nm(String pro_nm) {
		this.pro_nm = pro_nm;
	}

	public Date getPro_fromd() {
		return pro_fromd;
	}

	public void setPro_fromd(Date pro_fromd) {
		this.pro_fromd = pro_fromd;
	}

	public Date getPro_tod() {
		return pro_tod;
	}

	public void setPro_tod(Date pro_tod) {
		this.pro_tod = pro_tod;
	}

	public String getPro_loca() {
		return pro_loca;
	}

	public void setPro_loca(String pro_loca) {
		this.pro_loca = pro_loca;
	}

	public double getPro_loca_x() {
		return pro_loca_x;
	}

	public void setPro_loca_x(double pro_loca_x) {
		this.pro_loca_x = pro_loca_x;
	}

	public double getPro_loca_y() {
		return pro_loca_y;
	}

	public void setPro_loca_y(double pro_loca_y) {
		this.pro_loca_y = pro_loca_y;
	}

}
