/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package egovframework.com.comma.service.impl;

import java.util.List;
import java.util.Map;
import egovframework.com.comma.service.EgovSampleService;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @Class Name : EgovSampleServiceImpl.java
 * @Description : Sample Business Implement Class
 * @Modification Information
 *
 */

@Service("sampleService")
public class EgovSampleServiceImpl extends EgovAbstractServiceImpl implements EgovSampleService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EgovSampleServiceImpl.class);
	// mybatis 사용
	@Resource(name = "sampleMapper")
	private SampleMapper mapper;

	// @Override
	// public

	@Override
	public List<Map<String, Object>> setProData() throws Exception {
		LOGGER.debug(mapper.setProData().toString());
		return mapper.setProData();
	}

	@Override
	public List<Map<String, Object>> setTeamData() throws Exception {
		LOGGER.debug(mapper.setTeamData().toString());
		return mapper.setTeamData();
	}

	@Override
	public String getProNextID() throws Exception {
		LOGGER.debug(mapper.getProNextID());
		return mapper.getProNextID();
	}

	@Override
	public String getDeptNextID() throws Exception {
		LOGGER.debug(mapper.getDeptNextID());
		return mapper.getDeptNextID();
	}

	@Override
	public void updateProSheet(Map<String, Object> map) throws Exception {
		mapper.updateProSheet(map);
	}

	@Override
	public void insertProSheet(Map<String, Object> map) throws Exception {
		mapper.insertProSheet(map);
	}

	@Override
	public void deleteProSheet(Map<String, Object> map) throws Exception {
		mapper.deleteProSheet(map);
	}

	@Override
	public List<Map<String, Object>> getMemData() throws Exception {
		LOGGER.debug(mapper.getMemData().toString());
		return mapper.getMemData();
	}

	@Override
	public String getMemNextID() throws Exception {
		LOGGER.debug(mapper.getMemNextID());
		return mapper.getMemNextID();
	}

	@Override
	public void updateMemSheet(Map<String, Object> map) throws Exception {
		mapper.updateMemSheet(map);
	}

	@Override
	public void insertMemSheet(Map<String, Object> map) throws Exception {
		mapper.insertMemSheet(map);
	}

	@Override
	public void deleteMemSheet(Map<String, Object> map) throws Exception {
		mapper.deleteMemSheet(map);
	}

	@Override
	public List<Map<String, Object>> proListData() throws Exception {
		LOGGER.debug(mapper.proListData().toString());
		return mapper.proListData();
	}

	@Override
	public List<Map<String, Object>> getTeamData() throws Exception {
		LOGGER.debug(mapper.getTeamData().toString());
		return mapper.getTeamData();
	}

	@Override
	public List<Map<String, Object>> managerData() throws Exception {
		LOGGER.debug(mapper.managerData().toString());
		return mapper.managerData();
	}

	@Override
	public List<Map<String, Object>> deptCombo(String PRO_ID) throws Exception {
		LOGGER.debug(mapper.deptCombo(PRO_ID).toString());
		return mapper.deptCombo(PRO_ID);
	}

	@Override
	public void updateTeamSheet(Map<String, Object> map) throws Exception {
		mapper.updateTeamSheet(map);
	}

	@Override
	public void insertTeamSheet(Map<String, Object> map) throws Exception {
		mapper.insertTeamSheet(map);
	}

	@Override
	public void deleteTeamSheet(Map<String, Object> map) throws Exception {
		mapper.deleteTeamSheet(map);
	}

	@Override
	public void insertManSheet(Map<String, Object> map) throws Exception {
		mapper.insertManSheet(map);
	}

	@Override
	public void deleteManSheet(Map<String, Object> map) throws Exception {
		mapper.deleteManSheet(map);
	}
	@Override
	public List<Map<String, Object>> getProNM(String PRO_ID) throws Exception {
		LOGGER.debug(mapper.getProNM(PRO_ID).toString());
		return mapper.getProNM(PRO_ID);
	}

	@Override
	public List<Map<String, Object>> getChartData(String PRO_ID) throws Exception {
		LOGGER.debug(mapper.getChartData(PRO_ID).toString());
		return mapper.getChartData(PRO_ID);
	}

}
