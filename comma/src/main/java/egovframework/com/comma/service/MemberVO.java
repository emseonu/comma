/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package egovframework.com.comma.service;

public class MemberVO {

	private String mem_id;
	private String mem_nm;
	private String mem_sex;
	private String mem_img;
	private String mem_loca;
	private double mem_loca_y;
	private double mem_loca_x;
	public String getMem_id() {
		return mem_id;
	}
	public void setMem_id(String mem_id) {
		this.mem_id = mem_id;
	}
	public String getMem_nm() {
		return mem_nm;
	}
	public void setMem_nm(String mem_nm) {
		this.mem_nm = mem_nm;
	}
	public String getMem_sex() {
		return mem_sex;
	}
	public void setMem_sex(String mem_sex) {
		this.mem_sex = mem_sex;
	}
	public String getMem_img() {
		return mem_img;
	}
	public void setMem_img(String mem_img) {
		this.mem_img = mem_img;
	}
	public String getMem_loca() {
		return mem_loca;
	}
	public void setMem_loca(String mem_loca) {
		this.mem_loca = mem_loca;
	}
	public double getMem_loca_y() {
		return mem_loca_y;
	}
	public void setMem_loca_y(double mem_loca_y) {
		this.mem_loca_y = mem_loca_y;
	}
	public double getMem_loca_x() {
		return mem_loca_x;
	}
	public void setMem_loca_x(double mem_loca_x) {
		this.mem_loca_x = mem_loca_x;
	}



}
