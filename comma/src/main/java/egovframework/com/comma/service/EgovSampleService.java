/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package egovframework.com.comma.service;

import java.util.List;
import java.util.Map;

/**
 * @Class Name : EgovSampleService.java
 * @Description : EgovSampleService Class
 * @Modification Information
 */
public interface EgovSampleService {

	List<Map<String, Object>> setProData() throws Exception;

	List<Map<String, Object>> setTeamData() throws Exception;

	String getProNextID() throws Exception;

	String getDeptNextID() throws Exception;

	void updateProSheet(Map<String, Object> map) throws Exception;

	void insertProSheet(Map<String, Object> map) throws Exception;

	void deleteProSheet(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> getMemData() throws Exception;

	String getMemNextID() throws Exception;

	void updateMemSheet(Map<String, Object> map) throws Exception;

	void insertMemSheet(Map<String, Object> map) throws Exception;

	void deleteMemSheet(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> proListData() throws Exception;

	List<Map<String, Object>> getTeamData() throws Exception;

	List<Map<String, Object>> managerData() throws Exception;

	List<Map<String, Object>> deptCombo(String PRO_ID) throws Exception;

	void updateTeamSheet(Map<String, Object> map) throws Exception;

	void insertTeamSheet(Map<String, Object> map) throws Exception;

	void deleteTeamSheet(Map<String, Object> map) throws Exception;

	void deleteManSheet(Map<String, Object> map)throws Exception;

	void insertManSheet(Map<String, Object> map)throws Exception;

	List<Map<String, Object>> getProNM(String PRO_ID)throws Exception;

	List<Map<String, Object>> getChartData(String PRO_ID)throws Exception;

}
