<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<style>
.label {
	padding-left: 10px;
	font-size: 90%;
	vertical-align: 16px;
	color: #000;
	display: inline-block;
}
</style>
</head>
<body>
	<!-- modal -->
	<div class="modal" id="memPopup" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">구성원 정보</h4>
				</div>
				<div class="modal-body">
					<table class="table">
						<tbody>
							<tr>
								<td rowspan="5">
									<input type="image" id="photo" src="" style="width: 200px" />
									<br />
								</td>
								<td>
									<span class="label" id="mId1"></span>
								</td>
								<td>
									<span class="" id="mId1value"></span>
								</td>
							</tr>
							<tr>
								<td>
									<span class="label" id="mId2"></span>
								</td>
								<td>
									<span class="" id="mId2value"></span>
								</td>

							</tr>
							<tr>
								<td>
									<span class="label" id="mId3"></span>
								</td>
								<td>
									<span class="" id="mId3value"></span>
								</td>
							</tr>
							<tr>
								<td>
									<span class="label" id="mId4"></span>
								</td>
								<td>
									<span class="" id="mId4value"></span>
								</td>
							</tr>
							<tr>
								<td>
									<span class="label" id="mId5"></span>
								</td>
								<td>
									<span class="" id="mId5value"></span>
								</td>
							</tr>
						</tbody>
					</table>

				</div>
				<div class="modal-footer">
					
					<button type="button" class="btn btn-primary" data-dismiss="modal">확인</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>