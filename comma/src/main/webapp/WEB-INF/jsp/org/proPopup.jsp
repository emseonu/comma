<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<style>
.label {
	padding-left: 10px;
	font-size: 90%;
	vertical-align: middle;
	color: #000;
	display: inline-block;
}

.table>tbody>tr>td {
	vertical-align: middle;
}
</style>
</head>
<body>
	<!-- modal -->
	<div class="modal" id="proPopup" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 800px;">
				<div class="modal-header">
					<h4 class="modal-title">프로젝트 정보</h4>
				</div>
				<div class="modal-body" style="height: 550px;">
					<table class="table">
						<tbody>
							<tr>
								<td>
									<span class="label" id="pId1"></span>
								</td>
								<td>
									<span class="" id="pId1value"></span>
								</td>
							</tr>
							<tr>
								<td>
									<span class="label" id="pId2"></span>
								</td>
								<td>
									<span class="" id="pId2value"></span>
								</td>

							</tr>
							<tr>
								<td>
									<span class="label" id="pId4"></span>
								</td>
								<td>
									<span class="" id="pId4value"></span>
								</td>
							</tr>
							<tr>
								<td>
									<span class="label" id="pId5"></span>
								</td>
								<td>
									<span class="" id="pId5value"></span>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="col-sm-6" style="margin-top: 10px;">
						<div class="panel panel-default">
							<div class="panel-heading">
								<strong>구성원 성비</strong>
							</div>
							<div class="panel-body" id="sexChart" style="height: 280px;"></div>
						</div>
					</div>
					<div class="col-sm-6" style="margin-top: 10px;">
						<div class="panel panel-default">
							<div class="panel-heading">
								<strong>구성원 통근 거리</strong>
							</div>
							<div class="panel-body" id="distChart" style="height: 280px;"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" id="manage">관리</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">확인</button>
				</div>
			</div>
		</div>
	</div>
</body>


</html>