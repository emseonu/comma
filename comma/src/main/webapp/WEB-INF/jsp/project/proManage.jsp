<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<jsp:include page="../navbar/header.jsp"></jsp:include>
<script type="text/javascript" src="<c:url value='/js/project/sheet_view.js'/>"></script>
<style>
</style>

</head>
<body>

	<div class="row" style="height: 100%; margin: 0 !important;">

		<div class="col-md-10 col-md-offset-1" style="height: 97%;">
			<div class="col-6" style='padding-bottom: 10px; float: left;'>
				<div class="form-inline">
					<select class="form-control" id="dropdownPro">
						<option>프로젝트/부서명</option>
						<option>프로젝트ID</option>
						<option>위치</option>
					</select>
					<input type="text" class="form-control" id="sTextPro"></input>
					<button type="button" class="btn btn-default" id="searchPro">검색</button>
				</div>
			</div>

			<div class="col-6" style='padding-bottom: 10px; float: right;'>
				<button type="button" class="btn btn-default" id="insertPro">+ 프로젝트</button>
				<button type="button" class="btn btn-default" id="insertDept">+ 부서</button>
				<button type="button" class="btn btn-primary" id="save">저장</button>
			</div>
			<div id="sheetArea"></div>
		</div>
	</div>




</body>
</html>