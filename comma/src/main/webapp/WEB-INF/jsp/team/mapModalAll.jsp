<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<style>
#mapAll {
	width: 550px;
	height: 697px;
	float: left;
}

#list {
	width: 338px;
	padding: 0px;
	float: right;
	height: 700px;
	overflow: hidden;
	overflow-y: scroll;
}

#item {
	
}
</style>
</head>
<body>
	<!-- modal -->
	<div class="modal" id="modalPro" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document" style="width: 890px;">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body" style="height: 700px; padding: 0px;">
					<div class="inline">
						<div id="mapAll"></div>
						<div class="list-group" id="list">
							<a href="#" class="list-group-item" style="padding-top: 20px; padding-bottom: 20px;">
								<div class="inline">
									<input type="image" id="img" src="" style="height: 20px; margin-left: 10px; margin-bottom: 10px;" />
									<span class="label" id="NmAll"></span>
								</div> <br> <span id="All"></span>
							</a>
							<div id="manItem"></div>


							<div id="listItem"></div>

						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">확인</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>