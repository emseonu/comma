<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<style>
span {
	padding-top: 20px;
	padding-left: 35px;
}

.number2 {
	padding-left: 25px;
	vertical-align: 16px;
	font-weight: bold;
	color: #337ab7;
	height: 20px;
}

.distanceInfo {
	position: relative;
	top: 5px;
	left: 5px;
	list-style: none;
	margin: 0;
}

.label { //
	padding-left: 10px;
	font-size: 90%;
	vertical-align: 16px;
	color: #000;
	display: inline-block;
}

.distanceInfo:after {
	content: none;
}

.inline {
	position: absolute;
}

#map {
	width: 550px;
	height: 374px;
	float: left;
}

.list-group {
	width: 338px;
	/* 	background-color: #333; */
	float: right;
}
</style>
</head>
<body>
	<!-- modal -->
	<div class="modal" id="modalExample" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document" style="width: 890px;">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">통근거리 조회</h4>
				</div>
				<div class="modal-body" style="height: 375px; padding: 0px;">
					<div class="inline">
						<div id="map"></div>
						<div class="list-group" style="padding: 0px;">

							<a href="#" class="list-group-item" style="padding-top: 20px; padding-bottom: 20px;">
								<div class="inline">
									<input type="image" src="./img/mem.png" style="height: 20px; margin-left: 10px; margin-bottom: 10px;" />
									<span class="label" id="memNm"></span>
								</div> <br>
							<span id="mem"></span>
							</a> <a class="list-group-item"> <input type="image" src="./img/road.png" style="height: 25px; margin-left: 10px; margin-top: 10px; margin-bottom: 10px;" /> <span class="label">총거리</span><span class="number2" id="num1"></span>
							</a> <a class="list-group-item"> <input type="image" src="./img/car.png" style="height: 25px; margin-left: 10px; margin-top: 10px; margin-bottom: 10px;" /> <span class="label" style="margin-right: 10px; "> 차량</span><span class="number2" id="num2"></span>
							</a> <a class="list-group-item"> <input type="image" src="./img/bike.png" style="height: 25px; margin-left: 10px; margin-top: 10px; margin-bottom: 10px;" /> <span class="label">자전거</span><span class="number2" id="num3"></span>
							</a> <a href="#" class="list-group-item" style="padding-top: 20px; padding-bottom: 20px;">
								<div class="inline">
									<input type="image" src="./img/pro.png" style="height: 20px; margin-left: 10px; margin-bottom: 10px;" />
									<span class="label" id="proNm"></span>
								</div> <br> <span id="pro"></span>
							</a>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">확인</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>