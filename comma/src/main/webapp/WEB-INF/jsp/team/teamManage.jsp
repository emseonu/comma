<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<jsp:include page="../navbar/header.jsp"></jsp:include>
<script type="text/javascript" src="<c:url value='/js/team/sheet_view.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/team/sheet_view2.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/team/sheet_view3.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/team/sheet_view4.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/team/popup.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/team/popupSheet1.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/team/popupSheet2.js'/>"></script>

</head>
<body>
	<div class="row" style="height: 100%; margin: 0 !important;">
		<div class="row" style="height: 27%;">

			<div class="col-md-10 col-md-offset-1" style="height: 85%;">
				<strong>> 프로젝트 목록</strong>
				<div id="sheetArea1"></div>
			</div>
		</div>
		<div class="row" style="height: 70%;">
			<div class="col-md-4  col-md-offset-1" style="height: 97%; margin-top: 1%;">
				<strong>> 구성원 목록</strong>
				<div id="sheetArea2"></div>
			</div>
			<div class="col-md-6" style="height: 97%; margin-top: 1%;">
				<div style="display: inline;">
					<strong>> 프로젝트 관리</strong>
					<button type="button" class="btn btn-primary" id="save" style="float: right;">저장</button>
				</div>

				<div class="row" style="height: 97%;">
					<div class="col-md-12" style="height: 11%;">
						<a> 매니저 등록</a>
						<div id="sheetArea3"></div>
					</div>
					<div class="col-md-12" style="height: 75%; margin-top: 5%;">
						<a> 부서 및 구성원 등록</a>
						<div id="sheetArea4"></div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<jsp:include page="./mapModal.jsp"></jsp:include>
	<jsp:include page="./mapModalAll.jsp"></jsp:include>
<%-- 	<jsp:include page="./mapModalMem.jsp"></jsp:include>
 --%>
</body>
</html>