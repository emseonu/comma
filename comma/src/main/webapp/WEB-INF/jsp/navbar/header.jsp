<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko" xml:lang="ko">
<head>
<meta charset="UTF-8" />

<link rel="stylesheet" href="<c:url value='/softin/bootstrap/css/bootstrap.min.css'/>" />
<!-- 외부 라이브러리 -->
<script type="text/javascript" src="<c:url value='/softin/jquery/jquery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/softin/bootstrap/js/bootstrap.min.js'/>"></script>
<!-- INORG 제품 -->
<script type="text/javascript" src="<c:url value='/softin/softin.js'/>"></script>
<!-- 라이센스 정보 -->
<script type="text/javascript" src="<c:url value='/softin/inorg/inorginfo.js'/>"></script>
<!-- 생성시 필요한 함수 및 내부설정용 파일 -->
<script type="text/javascript" src="<c:url value='/softin/inorg/inorg.js'/>"></script>
<!-- core file -->
<script type="text/javascript" src="<c:url value='/softin/echarts.min.js'/>"></script>
<!-- IBSheet 제품 -->
<script type="text/javascript" src="<c:url value='/softin/ibsheet/ibsheet.js'/>"></script>
<script type="text/javascript" src="<c:url value='/softin/ibsheet/ibsheetinfo.js'/>"></script>
<!-- inapp -->
<script type="text/javascript" src="<c:url value='/softin/inapp.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app.js'/>"></script>

<!-- kakao api -->
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=f0e942ce112d1d2ede4300bc7be93005&libraries=services,clusterer,drawing"></script>
<style>
html, body {
	height: 95% !important;
}

.navbar-inverse .navbar-nav>li>a {
	color: #f5f5f5;
}
</style>
</head>
<body style="margin-top: 70px">
	<nav class="navbar  navbar-inverse navbar-fixed-top" style="background-color: #204161;">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="orgManage.do"> <img src="././img/comma.png" height="25" width="125" alt="logo"></a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<!--  <li class="active"><a href="#">Home</a></li> -->
				<li><a href="orgManage.do">조직도 관리</a></li>
				<li><a href="proManage.do">프로젝트 관리</a></li>
				<li><a href="memManage.do">구성원 관리</a></li>
				<li><a href="teamManage.do">프로젝트팀 관리</a></li>
			</ul>
		</div>
	</div>
	</nav>

	<!--

<nav class="navbar navbar-default navbar-fixed-top"  style="background-color: #e3f2fd;">

<a class="navbar-brand" href="#">
    <img src="././img/comma.png" height="30" width="120" alt="logo">
  </a>
<ul class="nav navbar-nav">
	<li><a href="orgManage.do">조직도 관리</a></li>
	<li><a href="proManage.do">프로젝트 관리</a></li>
	<li><a href="memManage.do">구성원 관리</a></li>
	<li><a href="teamManage.do">프로젝트팀 관리</a></li>
</ul>
</nav> -->

</body>
</html>