        inapp.add( "sheet_view3", {
            cfg: {},
            init: function () {
                this.create();
            },
            actionMap: {
                "sheet_view": {
                    "select_cell": function ( prop ) {
                        var pro_id, dept_id, pro_nm, proLocaX, proLocaY, p, proLoca;
                        p = prop.data.data;
                        pro_id = p.PRO_ID, dept_id = p.PRO_DEPT_KEY, proLocaY = p.PRO_LOCA_Y,
                            proLocaX = p.PRO_LOCA_X, pro_nm = p.PRO_NM, proLoca = p.PRO_LOCA;

                        this.filter( dept_id );
                        this.bind( pro_id, dept_id, pro_nm, proLoca, proLocaY, proLocaX );
                    },
                    "search_end": function ( prop ) {
                        var pro_id, dept_id, pro_nm, proLocaX, proLocaY, p, proLoca;
                        p = prop.data.data;
                        pro_id = p.PRO_ID, dept_id = p.PRO_DEPT_KEY, proLocaY = p.PRO_LOCA_Y,
                            proLocaX = p.PRO_LOCA_X, pro_nm = p.PRO_NM, proLoca = p.PRO_LOCA;

                        this.filter( dept_id );
                        this.bind( pro_id, dept_id, pro_nm, proLoca, proLocaY, proLocaX );
                    },
                    "button_clicked": function ( prop ) {
                        this.setLoca( prop.data.PRO_ID );
                    }
                },
                "sheet_view4": {
                    "save": function ( prop ) {
                        var data;
                        data = prop.data;

                        if ( data == "0" || data == "1" ) {
                            this.save( data );
                        }
                    }
                }
            },
            setLoca: function ( pro_id ) {
                var allRowCnt, id, locaD, i, counter, sStatus;
                counter = 0;
                allRowCnt = teamSheet3.RowCount();

                for ( i = 2; i <= allRowCnt + 1; i++ ) {
                    counter++;
                    id = teamSheet3.GetCellValue( i, "PRO_ID" );
                    sStatus = teamSheet3.GetCellValue( i, "sStatus" );
                    if ( id == pro_id && sStatus != "D" ) {
                        locaD = teamSheet3.GetRowJson( i );
                    }

                    if ( counter == allRowCnt ) {
                        inapp.raise( "sheet_view3", {
                            action: "set_loca",
                            data: locaD
                        } );
                    }
                }
            },
            createSheet: function () {
                createIBSheet2( document.getElementById( "sheetArea3" ), "teamSheet3", "100%", "100%" );
            },
            filter: function ( data ) {
                teamSheet3.SetFilterValue( "PRO_DEPT_KEY", data, 1 ); // 1 : 같다
                teamSheet3.ShowFilterRow();
                teamSheet3.SetRowHidden( 1, 1 ); // 1행 숨김
            },
            create: function () {
                var option, that;
                option = {}, that = this;
                this.createSheet();

                option = {
                    Cfg: { // 시트 기본 설정
                        SearchMode: smLazyLoad, // 조회방식설정 : 스크롤 조회
                        Page: 30, // 스크롤 조회에서 한번에 표시할 행의 개수
                        AutoFitColWidth: "search|resize|init",
                        "MergeSheet": msHeaderOnly,
                        "DragMode": 1
                    },
                    HeaderMode: { // header의 모드설정(SetHeaderMode
                        // Method)
                        Sort: 0,
                        ColMove: 0,
                        ColResize: 1, // type : Boolean, 컬럼 너비 resize 여부
                        HeaderCheck: 0
                    },
                    Cols: [ {
                        Header: "매니저ID",
                        Type: "Text",
                        Width: 20,
                        SaveName: "MEM_ID",
                        Align: "Center",
                        Edit: 0,
                        Hidden: 0
                    }, {
                        Header: "상태",
                        Type: "Status",
                        Align: "Center",
                        SaveName: "sStatus",
                        Width: 15,
                        Hidden: 0
                    }, {
                        Header: "삭제",
                        Type: "DelCheck",
                        Align: "Center",
                        SaveName: "sDelCheck",
                        Width: 15
                    }, {
                        Header: "성명",
                        Type: "Text",
                        Width: 20,
                        SaveName: "MEM_NM",
                        Align: "Center",
                        Edit: 0,
                        Hidden: 0
                    }, {
                        Header: "거주지",
                        Type: "Text",
                        Width: 60,
                        SaveName: "MEM_LOCA",
                        Edit: 0,
                        Hidden: 0
                    }, {
                        Header: "직선거리",
                        Type: "Float",
                        Width: 20,
                        SaveName: "TE_DISTANCE",
                        Format: "#0.#0 km",
                        Edit: 0,
                        Hidden: 0
                    }, {
                        Header: "통근거리",
                        Width: 20,
                        Type: "Button",
                        SaveName: "sButton",
                        Align: "Center",
                        Edit: 1,
                        Hidden: 0
                    }, {
                        Header: "프로젝트ID",
                        Width: 20,
                        Type: "Text",
                        SaveName: "PRO_ID",
                        Align: "Center",
                        Edit: 0,
                        Hidden: 1
                    }, {
                        Header: "부서ID",
                        Width: 20,
                        Type: "Text",
                        SaveName: "PRO_DEPT_KEY",
                        Align: "Center",
                        Edit: 0,
                        Hidden: 1
                    }, {
                        Header: "MEM_LOCA_Y",
                        Type: "Text",
                        Width: 60,
                        SaveName: "MEM_LOCA_Y",
                        Edit: 0,
                        Hidden: 1
                    }, {
                        Header: "MEM_LOCA_X",
                        Type: "Text",
                        Width: 60,
                        SaveName: "MEM_LOCA_X",
                        Edit: 0,
                        Hidden: 1
                    } ]
                }
                IBS_InitSheet( teamSheet3, option );
                teamSheet3.SetWaitImageVisible( 0 );
                teamSheet3.SetEditableColorDiff( 0 );
                teamSheet3.SetTheme( "TM", "TreeMain" );
                teamSheet3.SetFocusAfterProcess( 0 );

                this.getData();
            },
            getData: function () {
                var managerJson, managerData;
                managerData = {};
                // 매니저목록
                $.ajax( {
                    type: "POST",
                    url: "managerData.do",
                    dataType: "json",
                    contentType: "application/json;charset=UTF-8",
                    async: false, // json = data위함
                    success: function ( data ) {
                        managerJson = data;
                    },
                    error: function ( xhr, status, error ) {
                        console.log( error, status );
                    }
                } );

                for ( k in managerJson ) {
                    managerJson[ k ].sButton = "조  회 <img src='img/pin.png',width='14', height='14'>";
                }
                managerData[ "data" ] = managerJson;
                this.load( managerData ); // data load
            },

            save: function ( data ) {
                var that, s, rst, saveJson;
                s = teamSheet3, that = this;
                saveJson = s.GetSaveJson();

                if ( data == "0" && saveJson.data.length <= 0 ) {
                    if ( saveJson.Message == "NoTargetRows" ) {// 팀원 시트 저장내역 없음
                        alert( "저장할 내역이 없습니다." );
                        return;
                    }
                }

                if ( data == "1" && saveJson.data.length <= 0 ) { // 팀원 시트 저장내역 있음
                    if ( saveJson.Message == "NoTargetRows" ) {
                        rst = {
                            "Result": {
                                "Code": 0
                            }
                        };
                        s.LoadSaveData( rst );
                        return;
                    }
                }
                this.setAjax( saveJson );
            },
            setAjax: function ( saveJson ) {
                var data, s, rst;
                s = teamSheet3;
                data = JSON.stringify( saveJson );
             
                $.ajax( {   // 매니저 저장
                    type: "POST",
                    url: "saveManSheet.do",
                    data: data,
                    dataType: "json",
                    contentType: "application/json;charset=UTF-8",
                    success: function ( data ) {
                        rst = {
                            "Result": {
                                "Code": 0
                            }
                        };
                        s.LoadSaveData( rst );
                    },
                    error: function ( xhr, status, error ) {
                        console.log( error, status );
                        console.warn( xhr.responseText );
                        rst = {
                            "Result": {
                                "Code": 1
                            }
                        };
                        s.LoadSaveData( rst );
                    }
                } );
            },
            bind: function ( pro_id, dept_id, pro_nm, proLoca, proLocaY, proLocaX ) {
                // event
                var that;
                that = this;

                function addEvents( teamSheet3, callbacks ) {
                    for ( var prop in callbacks ) {
                        window[ teamSheet3 + "_" + prop ] = callbacks[ prop ];
                    }
                }
                addEvents( teamSheet3.id, {
                    OnSaveEnd: function ( code, msg, stCode, stMsg, responseText ) {
                        if ( code == 0 ) {
                            alert( "저장에 성공하였습니다." );
                        } else {
                            alert( "저장에 실패하였습니다." );
                        }
                    },
                    OnFilterEnd: function ( rowCnt, firstRow ) {
                    	var allRowCnt, i;
                        allRowCnt = teamSheet3.RowCount();

                        for ( i = 0; i <= allRowCnt + 1; i++ ) {
                            if ( teamSheet3.GetCellValue( i, "sStatus" ) == "D" ) {
                                teamSheet3.SetRowHidden( i, 1 );
                            }
                        }
                    },
                    OnButtonClick: function ( row, col ) {
                    	var json;
                        $( '#modalExample' ).modal( 'show' );
                        console.log( row, col );
                        json = teamSheet3.GetRowJson( row );

                        inapp.raise( "sheet_view3", {
                            action: "click_button",
                            data: {
                                data: json,
                                proNm: pro_nm,
                                proLoca: proLoca,
                                proLocaY: proLocaY,
                                proLocaX: proLocaX
                            }
                        } );
                    },
                    OnClick: function ( row ) {
                        var status;
                        status = teamSheet3.GetCellValue( row, "sStatus" );
                        console.log( status );
                        if ( status == "D" ) {
                            teamSheet3.SetRowHidden( row, 1 );
                        }
                    },
                    OnDropEnd: function ( FromSheet, FromRow, ToSheet, ToRow, X, Y, Type ) {
                    	var rowjson, allRowCnt, nhidCount, dept, delcheck, newRow, memLocaY, memLocaX, locaLine, path, distance, sDistance;
                      
                    	if ( FromSheet == ToSheet ){
                        	return;
                        }
                        rowjson = FromSheet.GetRowData( FromRow );

                        switch ( ToSheet.id ) {
                            case "teamSheet2":
                                break;
                            case "teamSheet3":
                                allRowCnt = ToSheet.RowCount();
                                nhidCount = 0;
                                for ( i = 2; i <= allRowCnt + 1; i++ ) {
                                    dept = ToSheet.GetCellValue( i, "PRO_DEPT_KEY" ); // 행 부서id
                                    delcheck = ToSheet.GetCellValue( i, "sDelCheck" ); // 행 hidden 여부
                                    if ( dept == dept_id && delcheck == 0 ) {
                                        nhidCount++; // hidden 아닌 부서 매니저 있는 경우
                                    }
                                }

                                if ( nhidCount > 0 ) { // hidden 보다 nhidden 이 많은 경우 -> 등록 매니저 있는 경우
                                    alert( "매니저는 최대 1명까지 등록될 수 있습니다." );
                                    return;
                                } else {
                                    ToSheet.SetRowData( ToRow + 1, rowjson, {
                                        "Add": 1
                                    } );

                                    newRow = ToSheet.GetSelectRow();
                                    memLocaY = teamSheet3.GetCellValue( newRow, "MEM_LOCA_Y" );
                                    memLocaX = teamSheet3.GetCellValue( newRow, "MEM_LOCA_X" );
                                    // 직선거리
                                    // 클릭한 위치를 기준으로 선을 생성하고 지도위에 표시합니다
                                    locaLine = new kakao.maps.Polyline( {
                                        path: [ new kakao.maps.LatLng( memLocaY, memLocaX ),
                                            new kakao.maps.LatLng( proLocaY, proLocaX )
                                        ]
                                    } );
                                    // 그려지고 있는 선의 좌표 배열을 얻어옵니다
                                    path = locaLine.getPath();
                                    distance = Math.round( locaLine.getLength() );
                                    sDistance = Math.round( distance / 100 ) / 10.0; // 소수점 첫째자리
                                    ToSheet.SetCellValue( newRow, "TE_DISTANCE", sDistance );
                                    ToSheet.SetCellValue( newRow, "PRO_ID", pro_id );
                                    ToSheet.SetCellValue( newRow, "PRO_DEPT_KEY", dept_id );
                                }
                                break;
                        }

                        if ( FromSheet.id == "teamSheet3" ) { // 행 데이터 숨기기
                            FromSheet.SetCellValue( FromRow, "sDelCheck", "1" );
                            FromSheet.SetRowHidden( FromRow, 1 );
                        }
                        if ( FromSheet.id == "teamSheet4" ) { // 행 데이터 숨기기
                            FromSheet.SetCellValue( FromRow, "sDelCheck", "1" );
                        }
                    }
                } );
            },
            load: function ( managerData ) {
                teamSheet3.LoadSearchData( managerData );

            }
        } )