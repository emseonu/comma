inapp.add("popupSheet1", {
	cfg : {
		manData : "",
		memData : ""
	},
	init : function() {
	},
	actionMap : {
		"sheet_view4" : {
			"set_loca" : function(prop) {
				if (prop.data.length == 0) {
					this.cfg.memData = null;
				}
				this.cfg.memData = prop.data;
			}
		},
		"sheet_view3" : {
			"set_loca" : function(prop) {
				if (typeof prop.data == "undefined") {
					this.cfg.manData = null;
				}
				this.cfg.manData = prop.data;
			}
		},
		"sheet_view" : {
			"button_clicked" : function(prop) {
				this.proData(prop.data, this.cfg.memData, this.cfg.manData);
			}
		}

	},

	proData : function(pro, mem, man) {
		var list, infowindow, iwContent, iwPosition, i, positions, imageSrc, imageSize, imageOption, markerImage, marker, mapContainer, mapOption, bounds, map;

		mapContainer = document.getElementById("mapAll");// 지도를 표시할 div
		mapOption = {
			center : new kakao.maps.LatLng(pro.PRO_LOCA_Y, pro.PRO_LOCA_X), // 지도의 중심좌표
			level : 6
		};
		positions = [];
		// 지도 생성
		map = new kakao.maps.Map(mapContainer, mapOption);
		bounds = new kakao.maps.LatLngBounds();
		imageSize = new kakao.maps.Size(35, 35);
		imageOption = {
			offset : new kakao.maps.Point(17, 37)
		};

		// 팀원 마커
		if (mem.length != 0) {
			for (i = 0; i < mem.length; i++) {
				positions.push({
					"title" : mem[i].MEM_NM,
					"latlng" : new kakao.maps.LatLng(mem[i].MEM_LOCA_Y, mem[i].MEM_LOCA_X)
				});
			}
			imageSrc = './img/mem.png';
			markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption);// 마커 이미지 생성
			console.log(positions);
			for (i = 0; i < positions.length; i++) {
				marker = new kakao.maps.Marker({
					map : map,
					position : positions[i].latlng, // 마커 표시할 위치
					title : positions[i].title, // 마커에 마우스 올리면 타이틀이 표시
					image : markerImage
				});
				bounds.extend(positions[i].latlng);
			}

			list = '';
			for (i = 0; i < positions.length; i++) {
				list += '<a href="#" class="list-group-item" id="item"  style="padding-top: 20px; padding-bottom: 20px;"><div class="inline"><input type="image" src="./img/mem.png" style="height: 20px; margin-left: 10px; margin-bottom: 10px;" /><span class="label" id="name">' + mem[i].MEM_NM + '</span></div> <br><span id="location">' + mem[i].MEM_LOCA + '</span></a>';
			}
			document.getElementById('listItem').innerHTML = list;
		} else {
			$('#listItem').empty();
		}

		// 매니저 마커
		if (typeof man != "undefined") {
			imageSrc = './img/manager.png';
			markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption);
			marker = new kakao.maps.Marker({
				map : map,
				position : new kakao.maps.LatLng(man.MEM_LOCA_Y, man.MEM_LOCA_X),
				title : man.MEM_NM,
				image : markerImage
			});
			$('#manName').html(" 매니저 | " + man.MEM_NM);
			$('#manLocation').html(man.MEM_LOCA);
			manager = '<a href="#" class="list-group-item" style="padding-top: 20px; padding-bottom: 20px;"><div class="inline"><input type="image" src="./img/manager.png" style="height: 20px; margin-left: 10px; margin-bottom: 10px;" /><span class="label" id="manName">매니저 |' + man.MEM_NM + '</span></div> <br> <span id="manLocation">' + man.MEM_LOCA + '</span></a>';
			bounds.extend(new kakao.maps.LatLng(man.MEM_LOCA_Y, man.MEM_LOCA_X));
			document.getElementById('manItem').innerHTML = manager;

		} else {
			$('#manItem').empty();
		}

		// 프로젝트 마커
		imageSrc = './img/pro.png';
		markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption);
		marker = new kakao.maps.Marker({
			map : map,
			position : new kakao.maps.LatLng(pro.PRO_LOCA_Y, pro.PRO_LOCA_X),
			title : pro.PRO_NM,
			image : markerImage

		});

		iwContent = '<div style="padding:5px; width:170px;">' + pro.PRO_NM + '<br>'; // 인포윈도우 내용
		iwPosition = new kakao.maps.LatLng(pro.PRO_LOCA_Y, pro.PRO_LOCA_X); // 인포윈도우 위치

		infowindow = new kakao.maps.InfoWindow({// 인포윈도우 생성
			position : iwPosition,
			content : iwContent
		});
		infowindow.open(map, marker);

		bounds.extend(new kakao.maps.LatLng(pro.PRO_LOCA_Y, pro.PRO_LOCA_X));// 지도안에 마커 다 보이도록
		map.setBounds(bounds);

		$('h4.modal-title').text("팀원위치 조회");
		$("#img").attr("src", './img/pro.png');
		$('#NmAll').html("프로젝트 | " + pro.PRO_NM);
		$('#All').html(pro.PRO_LOCA);

	}
})