inapp.add( "sheet_view4", {
    cfg: {},
    init: function () {
        this.create();
        this.button();

    },
    actionMap: {
        "sheet_view": {
            "select_cell": function ( prop ) {
                var p, pro_id, pro_nm, deptData, proLocaX, proLocaY, proLoca;
                p = prop.data.data;
                deptData = prop.data.dept_list, pro_id = p.PRO_ID, proLocaY = p.PRO_LOCA_Y,
                    proLocaX = p.PRO_LOCA_X, pro_nm = p.PRO_NM, proLoca = p.PRO_LOCA;

                this.filter( pro_id );
                this.bind( pro_id, pro_nm, deptData, proLoca, proLocaY, proLocaX );
            },
            "search_end": function ( prop ) {
                var p, pro_id, pro_nm, deptData, proLocaX, proLocaY, proLoca;
                p = prop.data.data;
                deptData = prop.data.dept_list, pro_id = p.PRO_ID, proLocaY = p.PRO_LOCA_Y,
                    proLocaX = p.PRO_LOCA_X, pro_nm = p.PRO_NM, proLoca = p.PRO_LOCA;

                this.filter( pro_id );
                this.bind( pro_id, pro_nm, deptData, proLoca, proLocaY, proLocaX );
            },
            "button_clicked": function ( prop ) {
                console.log( prop.data );
                this.setLoca( prop.data.PRO_ID );
            }
        }
    },
    setLoca: function ( pro_id ) {
        var allRowCnt, id, locaD, sStatus, i, counter;
        counter = 0;
        allRowCnt = teamSheet4.RowCount();
        locaD = [];
        for ( i = 2; i <= allRowCnt + 1; i++ ) {
            counter++;
            id = teamSheet4.GetCellValue( i, "PRO_ID" );
            sStatus = teamSheet4.GetCellValue( i, "sStatus" );
            if ( id == pro_id && sStatus != "D" ) {
                locaD.push( teamSheet4.GetRowJson( i ) );
            }

            if ( counter == allRowCnt ) {
                inapp.raise( "sheet_view4", {
                    action: "set_loca",
                    data: locaD
                } );
                console.log( locaD );
            }
        }

    },
    filter: function ( data ) {
        teamSheet4.SetFilterValue( "PRO_ID", data, 1 ); // 1 : 같다
        teamSheet4.ShowFilterRow();
    },
    createSheet: function () {
        createIBSheet2( document.getElementById( "sheetArea4" ), "teamSheet4", "100%", "100%" );
    },
    create: function () {
        var option, that;
        option = {}, that = this;
        this.createSheet();

        option = {
            Cfg: { // 시트 기본 설정
                SearchMode: smLazyLoad, // 조회방식설정 : 스크롤 조회
                Page: 30, // 스크롤 조회에서 한번에 표시할 행의 개수
                AutoFitColWidth: "search|resize|init",
                "MergeSheet": msHeaderOnly,
                "DragMode": 1

            },
            HeaderMode: { // header의 모드설정(SetHeaderMode
                // Method)
                Sort: 0,
                ColMove: 0,
                ColResize: 1, // type : Boolean, 컬럼 너비 resize 여부
                HeaderCheck: 0
            },
            Cols: [ {
                Header: "구성원ID",
                Type: "Text",
                Width: 20,
                SaveName: "MEM_ID",
                Align: "Center",
                Edit: 0,
                Hidden: 0
            }, {
                Header: "상태",
                Type: "Status",
                Align: "Center",
                SaveName: "sStatus",
                Width: 15
            }, {
                Header: "삭제",
                Type: "DelCheck",
                Align: "Center",
                SaveName: "sDelCheck",
                Width: 15
            }, {
                Header: "성명",
                Type: "Text",
                Width: 20,
                SaveName: "MEM_NM",
                Align: "Center",
                Edit: 0,
                Hidden: 0
            }, {
                Header: "부서명",
                Type: "Combo",
                Align: "Center",
                ComboText: "",
                ComboCode: "",
                Width: 20,
                SaveName: "PRO_NM",
                Edit: 1,
                Hidden: 0
            }, {
                Header: "거주지",
                Type: "Text",
                Width: 40,
                SaveName: "MEM_LOCA",
                Edit: 0,
                Hidden: 0
            }, {
                Header: "직선거리",
                Type: "Float",
                Width: 20,
                SaveName: "TE_DISTANCE",
                Format: "#0.#0 km",
                Edit: 0,
                Hidden: 0
            }, {
                Header: "통근거리",
                Width: 20,
                Type: "Button",
                SaveName: "sButton",
                Align: "Center",
                Edit: 1,
                Hidden: 0
            }, {
                Header: "프로젝트ID",
                Width: 20,
                Type: "Text",
                SaveName: "PRO_ID",
                Align: "Center",
                Edit: 0,
                Hidden: 1
            }, {
                Header: "부서ID",
                Width: 20,
                Type: "Text",
                SaveName: "PRO_DEPT_KEY",
                Align: "Center",
                Edit: 0,
                Hidden: 1
            }, {
                Header: "MEM_LOCA_Y",
                Type: "Text",
                Width: 60,
                SaveName: "MEM_LOCA_Y",
                Edit: 0,
                Hidden: 1
            }, {
                Header: "MEM_LOCA_X",
                Type: "Text",
                Width: 60,
                SaveName: "MEM_LOCA_X",
                Edit: 0,
                Hidden: 1
            } ]
        };
        IBS_InitSheet( teamSheet4, option );
        teamSheet4.SetWaitImageVisible( 0 );
        teamSheet4.SetEditableColorDiff( 0 );
        teamSheet4.SetTheme( "TM", "TreeMain" );
        teamSheet4.SetFocusAfterProcess( 0 );
        this.getData();
    },
    getData: function () {
        var teamJson, teamData, k;
        teamData = {};

        // 팀원목록
        $.ajax( {
            type: "POST",
            url: "getTeamData.do",
            dataType: "json",
            contentType: "application/json;charset=UTF-8",
            async: false, // json = data위함
            success: function ( data ) {
                teamJson = data;
            },
            error: function ( xhr, status, error ) {
                console.log( error, status );
            }
        } );
        for ( k in teamJson ) {
            teamJson[ k ].sButton = "조  회 <img src='img/pin.png',width='14', height='14'>";
        }
        teamData[ "data" ] = teamJson;

        this.load( teamData ); // data load
    },
    button: function () {
        var that;
        that = this;

        // 저장 버튼
        $( "#save" ).click( function () {
            that.save();
        } );

    },
    save: function () {
        var that, s, saveJson, counter;
        s = teamSheet4, counter = 0, that = this;

        saveJson = s.GetSaveJson();

        if ( saveJson.data.length <= 0 ) {
            if ( saveJson.Message == "NoTargetRows" ) {
                inapp.raise( "sheet_view4", {
                    action: "save",
                    data: "0"
                } );
                // alert( "저장할 내역이 없습니다." );
                return;
            }
        }

        // 주소
        saveJson.data.forEach( function ( d ) {
            if ( d.PRO_NM == "" && d.sStatus == "I" ) {
                alert( d.MEM_NM + "(" + d.MEM_ID + ")의 부서를 선택해 주세요." );
                return;
            }
            if ( d.PRO_NM != "" ) {
                d.PRE_DEPT = d.PRO_DEPT_KEY; // 이전 속해있던 부서
                d.PRO_DEPT_KEY = d.PRO_NM; // 현재 수정하는 부서
                counter++;
            } else {
                counter++;
            }
            if ( counter == saveJson.data.length ) {
                that.setAjax( saveJson );
            }
        } );

    },
    setAjax: function ( saveJson ) {
        var data, rst, s;
        s = teamSheet4;
        data = JSON.stringify( saveJson );

        // 프로젝트관리 수정 저장 : saveteamSheet
        $.ajax( {
            type: "POST",
            url: "saveTeamSheet.do",
            data: data,
            dataType: "json",
            contentType: "application/json;charset=UTF-8",
            success: function ( data ) {

                inapp.raise( "sheet_view4", {
                    action: "save",
                    data: "1"
                } );

                rst = {
                    "Result": {
                        "Code": 0
                    }
                };
                s.LoadSaveData( rst );

            },
            error: function ( xhr, status, error ) {
                console.log( error, status );
                console.warn( xhr.responseText );
                rst = {
                    "Result": {
                        "Code": 1
                    }
                };
                s.LoadSaveData( rst );
            }
        } );

    },
    bind: function ( pro_id, pro_nm, deptData, proLoca, proLocaY, proLocaX ) {
        // event
        var that;
        that = this;

        function addEvents( teamSheet4, callbacks ) {
            for ( var prop in callbacks ) {
                window[ teamSheet4 + "_" + prop ] = callbacks[ prop ];
            }
        }
        addEvents( teamSheet4.id, {
            OnFilterEnd: function ( rowCnt, firstRow ) {
                var allRowCnt, k, code, text, i, id;
                code = [], text = [];
                allRowCnt = teamSheet4.RowCount();
                for ( k in deptData ) {
                    code[ k ] = deptData[ k ].PRO_DEPT_KEY;
                    text[ k ] = deptData[ k ].PRO_NM;
                }

                for ( i = 0; i <= allRowCnt + 1; i++ ) {
                    id = teamSheet4.GetCellValue( i, "PRO_ID" );
                    if ( id == pro_id ) {
                        teamSheet4.CellComboItem( i, "PRO_NM", {
                            "ComboCode": code.join( "|" ),
                            "ComboText": text.join( "|" )
                        } );
                    }

                }
                teamSheet4.SetRowHidden( 1, 1 ); // 1행 숨김

            },
            OnButtonClick: function ( row, col ) {
                var json;
                $( '#modalExample' ).modal( 'show' );
                json = teamSheet4.GetRowJson( row );

                inapp.raise( "sheet_view4", {
                    action: "click_button",
                    data: {
                        data: json,
                        proNm: pro_nm,
                        proLoca: proLoca,
                        proLocaY: proLocaY,
                        proLocaX: proLocaX
                    }
                } );
            },
            OnDropEnd: function ( FromSheet, FromRow, ToSheet, ToRow, X, Y, Type ) {
                var i, k, rowjson, fromId, allRowCnt, toId, toDept, newRow, memLocaY, memLocax, locaLine, path, distance, sDistance, code, text;
                code = [], text = [];
                if ( FromSheet == ToSheet ) {
                    return;
                }

                rowjson = FromSheet.GetRowData( FromRow ); // 이미 포함된 구성원인지 검사
                fromId = FromSheet.GetCellValue( FromRow, "MEM_ID" );

                allRowCnt = ToSheet.RowCount();
                for ( i = 2; i <= allRowCnt + 1; i++ ) { // 동일한 프로젝트에 속해있는지 검사
                    toId = ToSheet.GetCellValue( i, "MEM_ID" );
                    toDept = ToSheet.GetCellValue( i, "PRO_ID" );
                    if ( fromId == toId && toDept == pro_id ) {
                        alert( "이미 존재하는 구성원입니다." );
                        return;
                    }
                }

                ToSheet.SetRowData( ToRow + 1, rowjson, { // 속해있지 않다면 행 추가
                    "Add": 1
                } );

                newRow = ToSheet.GetSelectRow(); // 새로 생긴 행에 부서 콤보 나타냄

                for ( k in deptData ) {
                    code[ k ] = deptData[ k ].PRO_DEPT_KEY;
                    text[ k ] = deptData[ k ].PRO_NM;
                }
                ToSheet.CellComboItem( newRow, "PRO_NM", {
                    "ComboCode": code.join( "|" ),
                    "ComboText": text.join( "|" )
                } );

                // 직선거리
                memLocaY = ToSheet.GetCellValue( newRow, "MEM_LOCA_Y" );
                memLocaX = ToSheet.GetCellValue( newRow, "MEM_LOCA_X" );

                locaLine = new kakao.maps.Polyline( { // 클릭한 위치를 기준으로 선을 생성하고 지도위에 표시합니다
                    path: [ new kakao.maps.LatLng( memLocaY, memLocaX ),
                        new kakao.maps.LatLng( proLocaY, proLocaX )
                    ]
                } );

                path = locaLine.getPath(); // 그려지고 있는 선의 좌표 배열을 얻어옵니다
                distance = Math.round( locaLine.getLength() );
                sDistance = Math.round( distance / 100 ) / 10.0; // 소수점 첫째자리
                ToSheet.SetCellValue( newRow, "TE_DISTANCE", sDistance );
                ToSheet.SetCellValue( newRow, "PRO_ID", pro_id ); // 출력

                if ( FromSheet.id == "teamSheet3" ) { // 행 데이터 숨기기
                    FromSheet.SetCellValue( FromRow, "sDelCheck", "1" );
                    FromSheet.SetRowHidden( FromRow, 1 );
                }
                if ( FromSheet.id == "teamSheet4" ) { // 행 데이터 숨기기
                    FromSheet.SetCellValue( FromRow, "sDelCheck", "1" );

                }
            }
        } );
    },

    load: function ( teamData ) {
        teamSheet4.LoadSearchData( teamData );

    }
} )