inapp.add("popup", {

	actionMap : {
		"sheet_view4" : {
			"click_button" : function(prop) {
				var p, m, mem_nm, memLoca, memLocaY, memLocaX, pro_nm, proLoca, proLocaY, proLocaX;
				m = prop.data.data, p = prop.data;
				mem_nm = m.MEM_NM, memLoca = m.MEM_LOCA, memLocaY = m.MEM_LOCA_Y, memLocaX = m.MEM_LOCA_X, pro_nm = p.proNm, proLoca = p.proLoca, proLocaY = p.proLocaY, proLocaX = p.proLocaX;

				this.show(mem_nm, memLoca, memLocaY, memLocaX, pro_nm, proLoca, proLocaY, proLocaX);
			}
		},
		"sheet_view3" : {
			"click_button" : function(prop) {
				var p, m, mem_nm, memLoca, memLocaY, memLocaX, pro_nm, proLoca, proLocaY, proLocaX;
				m = prop.data.data, p = prop.data;
				mem_nm = m.MEM_NM, memLoca = m.MEM_LOCA, memLocaY = m.MEM_LOCA_Y, memLocaX = m.MEM_LOCA_X, pro_nm = p.proNm, proLoca = p.proLoca, proLocaY = p.proLocaY, proLocaX = p.proLocaX;

				this.show(mem_nm, memLoca, memLocaY, memLocaX, pro_nm, proLoca, proLocaY, proLocaX);
			}
		}
	},
	show : function(mem_nm, memLoca, memLocaY, memLocaX, pro_nm, proLoca, proLocaY, proLocaX) {
		var imageSrc, imageSize, imageOption, markerImage, markerPosition, marker, mapContainer, mapOption, bounds, map, locaLine, distanceOverlay, dots, path, distance, content;
		mapContainer = document.getElementById('map'); // 지도를 표시할 div
		mapOption = {
			center : new kakao.maps.LatLng(37.5454575, 126.9902149), // 지도의 중심좌표
			level : 6
		};
		bounds = new kakao.maps.LatLngBounds();
		map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성
		locaLine = new kakao.maps.Polyline({
			map : map, // 선을 표시할 지도
			path : [ new kakao.maps.LatLng(memLocaY, memLocaX), new kakao.maps.LatLng(proLocaY, proLocaX) ], // 선을 구성하는 좌표 배열
			strokeWeight : 3, // 선의 두께
			strokeColor : '#337AE4', // 선의 색
			strokeOpacity : 1, // 선의 불투명도
			strokeStyle : 'solid' // 선의 스타일
		});

		// 프로젝트 마커
		imageSize = new kakao.maps.Size(35, 35); // 마커이미지의 크기
		imageOption = {
			offset : new kakao.maps.Point(17, 37)
		}; // 마커이미지 옵션. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정
		imageSrc = './img/pro.png'; // 마커이미지 주소
		markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption);// 마커이미지를 생성
		markerPosition = new kakao.maps.LatLng(proLocaY, proLocaX); // 마커 표시 위치

		marker = new kakao.maps.Marker({// 마커를 생성
			position : markerPosition,
			image : markerImage
		});
		marker.setMap(map);// 마커가 지도 위에 표시되도록 설정

		// 멤버 마커
		imageSrc = './img/mem.png';
		markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption);
		markerPosition = new kakao.maps.LatLng(memLocaY, memLocaX);

		marker = new kakao.maps.Marker({
			position : markerPosition,
			image : markerImage
		});

		marker.setMap(map);// 마커가 지도 위에 표시되도록 설정합니다

		path = locaLine.getPath();
		locaLine.setPath(path);
		distance = Math.round(locaLine.getLength());
		this.getTimeHTML(distance);

		bounds.extend(new kakao.maps.LatLng(memLocaY, memLocaX)); // 검색된 장소 위치를 기준으로 지도 범위를 재설정합니다
		bounds.extend(new kakao.maps.LatLng(proLocaY, proLocaX));

		map.setBounds(bounds);
		$('#proNm').html("프로젝트 | " + pro_nm);
		$('#memNm').html("팀원 | " + mem_nm);
		$('#pro').html(proLoca);
		$('#mem').html(memLoca);

	},
	getTimeHTML : function(distance) {
		var carkTime, carHour, bycicleTime, bycicleHour, content;
		// 도보의 시속은 평균 4km/h 이고 도보의 분속은 67m/min입니다
		// 차량으로 변경, 50 --> 833
		carkTime = distance / 833 | 0;
		carHour = '', carMin = '';

		// 계산한 도보 시간이 60분 보다 크면 시간으로 표시합니다
		// 차량으로 변경
		if (carkTime > 60) {
			carHour = +Math.floor(carkTime / 60) + '시간 '
		}
		carMin = carkTime % 60 + '분'

		// 자전거의 평균 시속은 16km/h 이고 이것을 기준으로 자전거의 분속은 267m/min입니다
		bycicleTime = distance / 227 | 0;
		bycicleHour = '', bycicleMin = '';

		// 계산한 자전거 시간이 60분 보다 크면 시간으로 표출합니다
		if (bycicleTime > 60) {
			bycicleHour = Math.floor(bycicleTime / 60) + '시간 '
		}
		bycicleMin = bycicleTime % 60 + '분'

		if (distance > 1000) {
			distance = distance / 1000 + "km";
		} else {
			distance = distance + "m";
		}

		$('#num1').html(distance);
		$('#num2').html(carHour + carMin);
		$('#num3').html(bycicleHour + bycicleMin);
	}
})