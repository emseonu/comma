inapp.add("sheet_view2", {
	cfg : {},
	init : function() {
		this.create();
	},
	actionMap : {},
	createSheet : function() {
		// sheetArea에 teamSheet를 100%로 생성
		createIBSheet2(document.getElementById("sheetArea2"), "teamSheet2", "100%", "100%");
	},
	create : function() {
		var option, that;
		option = {};
		that = this;
		this.createSheet();

		option = {
			Cfg : { // 시트 기본 설정
				SearchMode : smLazyLoad, // 조회방식설정 : 스크롤 조회
				Page : 30, // 스크롤 조회에서 한번에 표시할 행의 개수
				AutoFitColWidth : "search|resize|init",
				"MergeSheet" : msHeaderOnly,
				"DragMode" : 1
			},
			HeaderMode : { // header의 모드설정(SetHeaderMode
				// Method)
				Sort : 0,
				ColMove : 0,
				ColResize : 1, // type : Boolean, 컬럼 너비 resize 여부
				HeaderCheck : 0
			},
			Cols : [ {
				Header : "구성원ID",
				Type : "Text",
				Width : 20,
				SaveName : "MEM_ID",
				Align : "Center",
				Edit : 0,
				Hidden : 0
			}, {
				Header : "상태",
				Type : "Status",
				Align : "Center",
				SaveName : "sStatus",
				Width : 15,
				Hidden : 1
			}, {
				Header : "성명",
				Type : "Text",
				Width : 20,
				SaveName : "MEM_NM",
				Align : "Center",
				Edit : 0,
				Hidden : 0
			}, {
				Header : "거주지",
				Type : "Text",
				Width : 60,
				SaveName : "MEM_LOCA",
				Edit : 0,
				Hidden : 0
			}, {
				Header : "프로젝트 위치",
				Width : 20,
				Type : "Button",
				SaveName : "sButton",
				Align : "Center",
				Edit : 1,
				Hidden : 0
			}, {
				Header : "MEM_LOCA_Y",
				Type : "Text",
				Width : 60,
				SaveName : "MEM_LOCA_Y",
				Edit : 0,
				Hidden : 1
			}, {
				Header : "MEM_LOCA_X",
				Type : "Text",
				Width : 60,
				SaveName : "MEM_LOCA_X",
				Edit : 0,
				Hidden : 1
			} ]
		}

		IBS_InitSheet(teamSheet2, option);
		teamSheet2.ShowFilterRow();
		teamSheet2.SetEditableColorDiff(0);
		teamSheet2.SetTheme("TM", "TreeMain");
		teamSheet2.SetFocusAfterProcess(0);
		this.getData();
		this.bind(); // event

	},
	getData : function() {
		var memJson, memListData;
		memListData = {};
		// 구성원목록
		$.ajax({
			type : "POST",
			url : "getMemData.do",
			dataType : "json",
			contentType : "application/json;charset=UTF-8",
			async : false, // json = data위함
			success : function(data) {
				memJson = data;
			},
			error : function(xhr, status, error) {
				console.log(error, status);
			}
		});
		for (k in memJson) {
			memJson[k].sButton = "조  회 <img src='img/pin.png',width='14', height='14'>";
		}
		memListData["data"] = memJson;

		this.load(memListData); // data load
	},
	bind : function() {
		var that;
		that = this;

		function addEvents(teamSheet2, callbacks) {
			for ( var prop in callbacks) {
				window[teamSheet2 + "_" + prop] = callbacks[prop];
			}
		}
		addEvents(teamSheet2.id, {
			OnButtonClick : function(row, col) {
				var json;
				json = teamSheet2.GetRowJson(row);

				$('#modalPro').modal('show');
				inapp.raise("sheet_view2", {
					action : "button_clicked",
					data : json
				});
			},
			OnDropEnd : function(FromSheet, FromRow, ToSheet, ToRow, X, Y, Type) {
				var rowjson;
				if (FromSheet == ToSheet)
					return;
				FromSheet.GetRowData(FromRow);
				// 행 데이터 복사
				if (ToSheet.id != "teamSheet2") { // 행 데이터 복사
					ToSheet.SetRowData(ToRow + 1, rowjson, {
						"Add" : 1
					});
				}
				if (FromSheet.id == "teamSheet3") { // 행 데이터 숨기기
					FromSheet.SetCellValue(FromRow, "sDelCheck", "1");
					FromSheet.SetRowHidden(FromRow, 1);
				}
				if (FromSheet.id == "teamSheet4") { // 행 데이터 숨기기
					FromSheet.SetCellValue(FromRow, "sDelCheck", "1");
					// FromSheet.SetRowHidden( FromRow, 1 );
				}
			}
		});

	},

	load : function(memListData) {
		teamSheet2.LoadSearchData(memListData);
	}
})