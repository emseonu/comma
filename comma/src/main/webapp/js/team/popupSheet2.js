inapp.add("popupSheet2", {
	cfg : {
		proData : ""
	},
	init : function() {
	},
	actionMap : {
		"sheet_view" : {
			"set_loca" : function(prop) {
				if (prop.data.length == 0) {
					this.cfg.proData = null;
				}
				this.cfg.proData = prop.data;
			}
		},
		"sheet_view2" : {
			"button_clicked" : function(prop) {
				this.memData(prop.data, this.cfg.proData);
			}
		}
	},

	memData : function(mem, pro) {
		var list, infowindow, iwContent, iwPosition, i, positions, imageSrc, imageSize, imageOption, markerImage, marker, mapContainer, mapOption, bounds, map;

		mapContainer = document.getElementById("mapAll");// 지도를 표시할 div
		mapOption = {
			center : new kakao.maps.LatLng(mem.MEM_LOCA_Y, mem.MEM_LOCA_X), // 지도의 중심좌표
			level : 6
	
		};
		positions = [];
		map = new kakao.maps.Map(mapContainer, mapOption);
		bounds = new kakao.maps.LatLngBounds();

		imageSize = new kakao.maps.Size(35, 35);
		imageOption = {
			offset : new kakao.maps.Point(17, 37)
		};
		
		//프로젝트 마커
		if (pro.length != 0) {
			for (i = 0; i < pro.length; i++) {
				positions.push({
					"title" : pro[i].PRO_NM,
					"latlng" : new kakao.maps.LatLng(pro[i].PRO_LOCA_Y, pro[i].PRO_LOCA_X)
				});
			}
			imageSrc = './img/pro.png';
			markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption);
			for (i = 0; i < positions.length; i++) {
				marker = new kakao.maps.Marker({// 마커 생성
					map : map, 
					position : positions[i].latlng, 
					title : positions[i].title,
					image : markerImage
				// 마커 이미지
				});
				bounds.extend(positions[i].latlng);
			}

			list = '';
			for (i = 0; i < positions.length; i++) {
				list += '<a href="#" class="list-group-item" id="item"  style="padding-top: 20px; padding-bottom: 20px;"><div class="inline"><input type="image" src="./img/pro.png" style="height: 20px; margin-left: 10px; margin-bottom: 10px;" /><span class="label" id="name">' + pro[i].PRO_NM + '</span></div> <br><span id="location">' + pro[i].PRO_LOCA + '</span></a>';
			}
			document.getElementById('listItem').innerHTML = list;
		} else {
			$('#listItem').empty();
		}

		imageSrc = './img/mem.png';
		markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption);// 마커 이미지 생성
		marker = new kakao.maps.Marker({// 마커를 생성
			map : map,
			position : new kakao.maps.LatLng(mem.MEM_LOCA_Y, mem.MEM_LOCA_X), // 마커 표시 위치
			title : mem.MEM_NM,
			image : markerImage
		});

		iwContent = '<div style="padding:5px; width:150px; text-align: center;">' + mem.MEM_NM + '<br>'; // 인포윈도우 내용
		iwPosition = new kakao.maps.LatLng(mem.MEM_LOCA_Y, mem.MEM_LOCA_X); // 인포윈도우 표시 위치

		
		infowindow = new kakao.maps.InfoWindow({// 인포윈도우를 생성
			position : iwPosition,
			content : iwContent
		});
		infowindow.open(map, marker);

		bounds.extend(new kakao.maps.LatLng(mem.MEM_LOCA_Y, mem.MEM_LOCA_X));
		map.setBounds(bounds);
		
		$('#manItem').empty();
		$('h4.modal-title').text("프로젝트위치 조회");
		$("#img").attr("src", './img/mem.png');
		$('#NmAll').html(mem.MEM_NM);
		$('#All').html(mem.MEM_LOCA);

	}
})