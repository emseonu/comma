inapp.add("sheet_view", {
	cfg : {},
	init : function() {
		this.create();
	},
	actionMap : {

	},
	createSheet : function() {
		createIBSheet2(document.getElementById("sheetArea1"), "teamSheet1", "100%", "100%");
	},
	create : function() {
		var option, that;
		option = {}, that = this;
		this.createSheet();

		option = {
			Cfg : { // 시트 기본 설정
				SearchMode : smLazyLoad, // 조회방식설정 : 스크롤 조회
				Page : 30, // 스크롤 조회에서 한번에 표시할 행의 개수
				AutoFitColWidth : "search|resize|init",
				MergeSheet : msHeaderOnly
			},
			HeaderMode : { // header의 모드설정(SetHeaderMode
				// Method)
				Sort : 0,
				ColMove : 0,
				ColResize : 1, // type : Boolean, 컬럼 너비 resize 여부
				HeaderCheck : 0
			},
			Cols : [ {
				Header : "프로젝트ID",
				Type : "Text",
				Width : 20,
				SaveName : "PRO_ID",
				Align : "Center",
				Edit : 0,
				Hidden : 0
			}, {
				Header : "프로젝트명",
				Type : "Text",
				Width : 40,
				SaveName : "PRO_NM",
				Align : "Center",
				Edit : 0,
				Hidden : 0
			}, {
				Header : "위치",
				Type : "Text",
				Width : 40,
				SaveName : "PRO_LOCA",
				Edit : 0,
				Hidden : 0
			}, {
				Header : "팀원 위치",
				Width : 20,
				Type : "Button",
				SaveName : "sButton",
				Align : "Center",
				Edit : 1,
				Hidden : 0
			}, {
				Header : "부서ID",
				Width : 20,
				Type : "Text",
				SaveName : "PRO_DEPT_KEY",
				Align : "Center",
				Edit : 0,
				Hidden : 1
			}, {
				Header : "PRO_LOCA_Y",
				Type : "Text",
				Width : 60,
				SaveName : "PRO_LOCA_Y",
				Edit : 0,
				Hidden : 1
			}, {
				Header : "PRO_LOCA_X",
				Type : "Text",
				Width : 60,
				SaveName : "PRO_LOCA_X",
				Edit : 0,
				Hidden : 1
			} ]
		}
		IBS_InitSheet(teamSheet1, option);
		teamSheet1.ShowFilterRow();
		teamSheet1.SetEditableColorDiff(0);
		teamSheet1.SetTheme("TM", "TreeMain");
		this.getData();// get data
		this.bind(); // event
	},
	getData : function() {
		var proJson, proListData;
		proListData = {};

		$.ajax({// 프로젝트목록
			type : "POST",
			url : "proListData.do",
			dataType : "json",
			contentType : "application/json;charset=UTF-8",
			async : false, // json = data위함
			success : function(data) {
				proJson = data;
			},
			error : function(xhr, status, error) {
				console.log(error, status);
			}
		});

		inapp.raise("sheet_view", { // marker 위해 위치 저장
			action : "set_loca",
			data : proJson
		});

		for (k in proJson) {
			proJson[k].sButton = "조  회 <img src='img/pin.png',width='14', height='14'>";
		}
		proListData["data"] = proJson;
		this.load(proListData); // data load
		
	},
	bind : function() {
		var that;
		that = this;

		function addEvents(teamSheet1, callbacks) {
			for ( var prop in callbacks) {
				window[teamSheet1 + "_" + prop] = callbacks[prop];
			}
		}
		addEvents(teamSheet1.id, {
			OnSelectCell : function(OldRow, OldCol, NewRow, NewCol, isDelete) {
				var proId, json, set, pro_dept;
				proId = teamSheet1.GetCellValue(teamSheet1.GetSelectRow(), "PRO_ID");
				json = teamSheet1.GetRowJson(teamSheet1.GetSelectRow());
				set = {
					"PRO_ID" : proId
				};
				$.ajax({
					type : "POST",
					url : "deptCombo.do",
					data : JSON.stringify(set),
					dataType : "json",
					contentType : "application/json;charset=UTF-8",
					async : false, // json = data위함
					success : function(data) {
						pro_dept = data;
					},
					error : function(xhr, status, error) {
						console.log(error, status);
					}
				});

				inapp.raise("sheet_view", {
					action : "select_cell",
					data : {
						data : json,
						dept_list : pro_dept
					}
				});
			},
			OnSearchEnd : function(code, msg, stCode, stMsg, responseText) {
				var proId, json, set, pro_dept;
				proId = teamSheet1.GetCellValue(teamSheet1.GetSelectRow(), "PRO_ID");
				json = teamSheet1.GetRowJson(teamSheet1.GetSelectRow());

				set = {
					"PRO_ID" : proId
				};
				$.ajax({
					type : "POST",
					url : "deptCombo.do",
					data : JSON.stringify(set),
					dataType : "json",
					contentType : "application/json;charset=UTF-8",
					async : false, // json = data위함
					success : function(data) {
						pro_dept = data;
					},
					error : function(xhr, status, error) {
						console.log(error, status);
					}
				});

				inapp.raise("sheet_view", {
					action : "search_end",
					data : {
						data : json,
						dept_list : pro_dept
					}
				});
			},
			OnButtonClick : function(row, col) {
				var json;
				json = teamSheet1.GetRowJson(row);

				$('#modalPro').modal('show');
				inapp.raise("sheet_view", {
					action : "button_clicked",
					data : json
				});
			}
		});
	},

	load : function(proListData) {
		teamSheet1.LoadSearchData(proListData);
	}
})