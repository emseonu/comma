inapp.add("sheet_view", {
	cfg : {},
	init : function() {
		this.create();
		this.button();
	},
	actionMap : {

	},
	createSheet : function() {
		// sheetArea에 proSheet를 100%로 생성
		createIBSheet2(document.getElementById("sheetArea"), "proSheet", "100%", "100%");
	},
	create : function() {
		// 개발자 가이드 - 5.1 항목을 보고 작성
		var option, that;
		option = {}, that = this;
		this.createSheet();

		option = {
			Cfg : { // 시트 기본 설정
				SearchMode : smLazyLoad, // 조회방식설정 : 스크롤 조회
				Page : 30, // 스크롤 조회에서 한번에 표시할 행의 개수
				AutoFitColWidth : "search|resize|init",
				MergeSheet : msPrevColumnMerge + msHeaderOnly
			},
			HeaderMode : { // header의 모드설정(SetHeaderMode
				// Method)
				Sort : 0,
				ColMove : 0,
				ColResize : 1, // type : Boolean, 컬럼 너비 resize
				// 여부
				HeaderCheck : 0
			},
			Cols : [ {
				Header : "프로젝트ID",
				Type : "Text",
				Width : 20,
				SaveName : "PRO_ID",
				Align : "Center",
				Edit : 0,
				Hidden : 0
			}, {
				Header : "부서ID",
				Type : "Text",
				Width : 20,
				SaveName : "PRO_DEPT_KEY",
				Align : "Center",
				Edit : 0,
				Hidden : 0
			}, {
				Header : "PRO_UPID",
				Type : "Text",
				Width : 50,
				SaveName : "PRO_UPID",
				Edit : 0,
				Hidden : 1
			}, {
				Header : "상태",
				Type : "Status",
				Align : "Center",
				SaveName : "sStatus",
				Width : 15
			}, {
				Header : "삭제",
				Type : "DelCheck",
				Align : "Center",
				SaveName : "sDelCheck",

				Width : 15
			}, {
				Header : "프로젝트/부서명",
				Type : "Text",
				Width : 60,
				SaveName : "PRO_NM",
				Align : "Left",
				Edit : 1,
				Hidden : 0,
				TreeCol : 1
			}, {
				Header : "시작일",
				Type : "Date",
				Width : 25,
				SaveName : "PRO_FROMD",
				Align : "Center",
				Edit : 0,
				Hidden : 0,

			}, {
				Header : "종료일",
				Type : "Date",
				Width : 25,
				SaveName : "PRO_TOD",
				Align : "Center",
				Edit : 0,
				Hidden : 0
			}, {
				Header : "위치",
				Type : "Text",
				Width : 60,
				SaveName : "PRO_LOCA",
				Edit : 0,
				Hidden : 0
			}, {
				Header : "PRO_LVL",
				Type : "Text",
				Width : 60,
				SaveName : "PRO_LVL",
				Edit : 0,
				Hidden : 1
			}, {
				Header : "PRO_LOCA_Y",
				Type : "Text",
				Width : 60,
				SaveName : "PRO_LOCA_Y",
				Edit : 0,
				Hidden : 1
			}, {
				Header : "PRO_LOCA_X",
				Type : "Text",
				Width : 60,
				SaveName : "PRO_LOCA_X",
				Edit : 0,
				Hidden : 1
			} ]
		}
		IBS_InitSheet(proSheet, option);
		proSheet.SetTheme("TM", "TreeMain");
		proSheet.SetTreeActionMode(1); // 삭제 선택 시 자식 행까지 모두 삭제
		proSheet.ShowFilterRow();
		proSheet.SetRowHidden(1, 1);
		proSheet.SetWaitImageVisible(0);

		this.getData(); // get data
		this.bind(); // event
	},
	getData : function() {
		var json, jsonData;
		jsonData = {};

		// 프로젝트관리 데이터 : getProData
		$.ajax({
			type : "POST",
			url : "setProData.do",
			dataType : "json",
			contentType : "application/json;charset=UTF-8",
			async : false, // json = data위함
			success : function(data) {
				json = data;
			},
			error : function(xhr, status, error) {
				console.log(error, status);
			}
		});

		for (k in json) { // tree위한 Level값 추가
			json[k].Level = json[k].PRO_LVL - 1;
		}
		jsonData["data"] = json;
		this.load(jsonData); // data load
	},
	button : function() {
		var that, dropdown, searchText;
		that = this;
		// 검색
		$("#searchPro").click(function() {
			dropdown = $("#dropdownPro").val();
			searchText = $("#sTextPro").val();
			that.search(dropdown, searchText);
		});

		// 검색 reset
		$("#dropdownPro").on("change", function() {
			$("#sTextPro").val("");
			proSheet.ClearFilterRow();
			proSheet.SetRowHidden(1, 1);
		});

		// 프로젝트 추가 버튼
		$("#insertPro").click(function() {
			that.insertPro();
		});

		// 부서 추가 버튼
		$("#insertDept").click(function() {
			that.insertDept();
		});

		// 저장 버튼
		$("#save").click(function() {
			that.save();
		});
	},
	search : function(dropdown, searchText) {
		var col, row;

		if (searchText == "") { // 검색어 없을경우 이전text clear, return
			proSheet.ClearFilterRow();
			proSheet.SetRowHidden(1, 1);
			return;
		}

		if (dropdown == "프로젝트/부서명") {
			col = "PRO_NM";
		} else if (dropdown == "프로젝트ID") {
			col = "PRO_ID";
		} else if (dropdown == "위치") {
			col = "PRO_LOCA";
		}

		proSheet.SetFilterValue(col, searchText, 11);

	},
	insertPro : function() {
		var row, s;
		s = proSheet;

		s.DataInsert(null, 1); // 1레벨 프로젝트 행 추가
		row = s.GetSelectRow(); // 해당 행에 시작, 종료, 위치 입력 가능
		s.SetCellEditable(row, 6, 1);
		s.SetCellEditable(row, 7, 1);
		s.SetCellEditable(row, 8, 1);
		$.post("getProNextID.do", function(data) { // get pro_id
			s.SetCellValue(row, 0, data);
		});

		$.post("getDeptNextID.do", function(data) { // get dept_id
			s.SetCellValue(row, 1, data);
		});
		s.SetCellValue(row, 2, "0001"); // pro_upid
		s.SetCellValue(row, 9, 2); // pro_lvl
	},
	insertDept : function() {
		var row, level, upId, s, parentRow, parentProId;
		s = proSheet;

		row = s.GetSelectRow();
		level = s.GetRowLevel(proSheet.GetSelectRow());
		upId = s.GetCellValue(row, 0);
		if (upId == "0001") { // 선택된 행이 최상위 프로젝트일 경우
			// return
			alert("최상위 프로젝트에는 부서를 추가할 수 없습니다.");
			return;
		}

		s.DataInsert(null, 2); // 부서행 추가
		row = s.GetSelectRow();
		parentRow = s.GetParentRow(row); // 포함되는 pro_id
		parentProId = s.GetCellValue(parentRow, 0);
		s.SetCellValue(row, 0, parentProId);
		$.post("getDeptNextID.do", function(data) { // dept_id
			s.SetCellValue(row, 1, data);
		});

		upId = s.GetCellValue(parentRow, 1);
		s.SetCellValue(row, 2, upId); // pro_upid
		s.SetCellValue(row, 9, 3); // pro_lvl

	},
	save : function() {
		var that, s, saveJson, x, y, geocoder, rst, d, counter, counterN, rst;
		d = {};
		s = proSheet;
		counter = 0, counterN = 0;
		that = this;
		// 주소-좌표 변환 객체를 생성합니다
		geocoder = new kakao.maps.services.Geocoder();
		saveJson = s.GetSaveJson();

		if (saveJson.data[0].PRO_ID == "0001") { // 선택된 행이 최상위 프로젝트일 경우
			// return
			alert("최상위 프로젝트는 삭제할 수 없습니다.");
			return;
		}
		if (saveJson.data.length <= 0) {
			if (saveJson.Message == "NoTargetRows") {
				alert("저장할 내역이 없습니다.");
				return;
			}
		}

		saveJson.data.forEach(function(d) {
			if (d.PRO_LVL == 2) {
				geocoder.addressSearch(d.PRO_LOCA, function(result, status) {
					// 정상적으로 검색이 완료됐으면
					if (status === kakao.maps.services.Status.OK) {
						console.log("loca : " + d.PRO_LOCA);
						d.PRO_LOCA_Y = result[0].y;
						d.PRO_LOCA_X = result[0].x;
					} else {
						alert(d.PRO_NM + "프로젝트에 정확한 주소를 입력해 주세요.");
						return;
					}
					counter++;
					if (counter == saveJson.data.length) {
						setAjax();
					}
				});
			} else {
				counter++;
				if (counter == saveJson.data.length) {
					setAjax();
				}
			}

		});

		function setAjax() {
			var data;
			data= JSON.stringify(saveJson);
			// 프로젝트관리 수정 저장 : saveProSheet
			$.ajax({
				type : "POST",
				url : "saveProSheet.do",
				data : data,
				dataType : "json",
				contentType : "application/json;charset=UTF-8",
				success : function(data) {
					console.log(data);

					rst = {
						"Result" : {
							"Code" : 0,
							"Message" : "저장 되었습니다."
						}
					};
					s.LoadSaveData(rst);

				},
				error : function(xhr, status, error) {
					console.log(error, status);
					console.warn(xhr.responseText);
				}
			});
		}
	},
	bind : function() {
		var that;
		that = this;

		function addEvents(proSheet, callbacks) {
			for ( var prop in callbacks) {
				window[proSheet + "_" + prop] = callbacks[prop];
			}
		}
		addEvents(proSheet.id, {
			OnClick : function(row) {
				var select_key;
				select_key = proSheet.GetCellValue(row, 0);
			},
			OnRowSearchEnd : function(row) {
				var upId;
				upId = proSheet.GetCellValue(row, 2);// 프로젝트 추가시에만 시작, 종료일, 위치 수정 가능
				if (upId == "0001") { // 부모값이 최상위프로젝트(0001)일 경우로 판단
					proSheet.SetCellEditable(row, 6, 1);
					proSheet.SetCellEditable(row, 7, 1);
					proSheet.SetCellEditable(row, 8, 1);
				}
			},
			OnSaveEnd : function(code) {
				if (code == 0) {
					alert("저장에 성공하였습니다.");
				} else {
					alert("저장에 실패하였습니다.");
				}
			}
		});
	},

	load : function(jsonData) {
		proSheet.LoadSearchData(jsonData);
	}
})