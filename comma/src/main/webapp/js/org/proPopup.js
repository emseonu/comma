inapp.add("proPopup", {
	init : function() {
		//	this.button();
	},
	actionMap : { // orgview 받아온 값으로 event처리
		"org_view" : {
			"click_node" : function(prop) {
				// / 반복해서 쓰일 것들은 하나의 변수에 넣어서 처리하는게 좋아요!!
				var data;
				data = prop.data;

				if (!data.binding)// binding 없으면 return
					return;

				if (data.key == "0001")// 최상위는 팝없 없음
					return;

				if (data.data.PRO_LOCA != null && data.binding == "PRO_NM") {
					this.showPro(data.data);
				}

			}
		}
	},
	/*	button : function() {
	 $("#manage").on("click", function() {
	 var id, url;
	 id = $("#pId1value").text();
	 url = "./proManage.do?id=" + id;
	 window.location.href = url;
	 });
	 $
	 },*/
	showPro : function(data) { // project popup
		var fromD, fYear, fMonth, fDay, toD, tYear, tMonth, tDay;

		$("#proPopup").modal("show");
		$("#pId1").html("프로젝트 ID");
		$("#pId1value").html(data.PRO_ID);
		$("#pId2").html("프로젝트명");
		$("#pId2value").html(data.PRO_NM);
		$("#pId4").html("기간");
		fromD = new Date(Number(data.PRO_FROMD));
		toD = new Date(Number(data.PRO_TOD));
		fYear = fromD.getFullYear();
		fMonth = fromD.getMonth() + 1;
		fDay = fromD.getDate();
		tYear = toD.getFullYear();
		tMonth = toD.getMonth() + 1;
		tDay = toD.getDate();

		console.log(fromD);
		$("#pId4value").html(fYear + " / " + fMonth + " / " + fDay + "~" + tYear + " / " + tMonth + " / " + tDay);
		$("#pId5").html("위치");
		$("#pId5value").html(data.PRO_LOCA);
	}

})