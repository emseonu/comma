inapp.add("org_view", {
	init : function() {
		this.create();
		this.click_sync();
		this.expand_sync();
		this.button();
	},
	actionMap : { // tree_view에서 받아온 값으로 event처리
		"tree_view" : {
			"click_tree" : function(prop) {
				this.click_sync(prop.data);
			},
			"expand_tree" : function(prop) {
				var data;
				data = prop.data;
				this.expand_sync(data.key, data.expand);
			}
		}
	},
	create : function() {
		var option, that;
		that = this; // inapp.raise에서 org_view 대신 쓰기위함

		option = {
			layout : {
				level : true,
				supporter : false
			},

			defaultTemplate : {
				link : { // 연결선 스타일
					style : {
						borderWidth : 2,
						borderColor : "#CCC",
						borderStyle : "solid"
					}
				}
			},
			event : {
				onClick : function(evt) {
					var select_key, node, json_data;

					// 노드가 선택되지 않았을 땐, 처리하지 않는다.
					// 이벤트가 노드관련해서만 있기 때문에.
					if (!evt.key) {
						return;
					}

					// 멤버 여부에 따라 노드를 구분해서 node 변수에 담는다.
					if (evt.isMember == true) {
						node = evt.member;
					} else {
						node = evt.node;
					}

					json_data = {};

					// 노드 필드 데이터를 datas에 저장한다.
					node.fields().foreach(function() {
						json_data[this.name()] = this.value();
					});

					inapp.raise(that.name, {
						action : "click_node",
						data : {
							key : evt.key,
							binding : evt.binding,
							data : json_data
						}
					});

				},
				onExpandButtonClick : function(evt) {
					inapp.raise(that.name, {
						action : "expand_node",
						data : {
							key : evt.key,
							expand : evt.isExpand
						}
					});
				},
				onDiagramLoadEnd : function(evt) {
					var jsonData, data, org, node, cnt, nodes, val;;
					// 조직도 데이터를 트리 데이터 형식으로 가공
					data = [];
					jsonData = {};

					function push(a) {
						data.push({
							"PRO_DEPT_KEY" : a.fields("PRO_DEPT_KEY").value(),
							"PRO_NM" : a.fields("PRO_NM").value(),
							"Level" : a.level() - 1
						});
					}

					// foreach를 통해 노드의 필드 데이터를 추출(라벨과 멤버 노드는 제외)
					evt.org.nodes().foreach(function() {
						if (!this.isLabel() && !this.isMember()) {
							// 최상위 프로젝트
							if (this.fields("PRO_UPID").value() == null) {
								push(this);
							}// 2레벨 프로젝트, 해당 자식
							if (this.fields("PRO_UPID").value() == 0001) {
								push(this);
								this.descendant().foreach(function() {
									push(this);
								});

							}
						}
					});

					jsonData["data"] = data;

					inapp.raise(that.name, {
						action : "load_node",
						data : jsonData
					});

					// 인원 카운트
					org = evt.org;

					nodes = org.nodes().filter(function() {
						return this.treeLevel() === 2;
					});

					// 팀 갯수와 팀 내 인원수
					nodes.foreach(function() {
						node = this;

						cnt = 0;

						node.children().foreach(function() {
							cnt += this.members().length;
						});
						node.members().fields("COUNT_DEPT").value(node.children().length);
						node.members().fields("COUNT_MEM").value(cnt);
						console.log(node.fields("PRO_NM").value(), "팀 갯수", node.children().length, "인원수", cnt);
					});

				}
			}
		}
		createINOrg("viewOrg", option);

		this.getNodeData();

	},
	getNodeData : function() {
		var that, k, json, nodeData, teamJson, template, pro_id_arr, cnt, val, i, v, ran;
		k = 0, nodeData = [], teamData = [], pro_id_arr = [];
		that = this;
		// controller통해 project의 모든 data 받아옴
		$.ajax({
			type : "POST",
			url : "setProData.do",
			dataType : "json",
			contentType : "application/json;charset=UTF-8",
			async : false, // json = data위함
			success : function(data) {
				json = data;
			},
			error : function(xhr, status, error) {
				console.log(error, status);
			}
		});

		// ajax로 받아온 data를 json에 담아 org형식으로 맞춤
		for (k in json) {

			if (json[k].PRO_LVL == 1) {
				template = "OrgBox";
			} else if (json[k].PRO_LVL == 2) {
				template = "PmBox";
				pro_id_arr.push(json[k].PRO_DEPT_KEY);
			} else {
				template = "listTemp";
			}

			nodeData.push({
				"layout" : {
					"level" : json[k].PRO_LVL
				},
				"template" : template,
				"fields" : json[k]
			});
		}

		$.ajax({
			type : "POST",
			url : "setTeamData.do",
			dataType : "json",
			contentType : "application/json;charset=UTF-8",
			async : false,
			success : function(data) {
				teamJson = data;
			},
			error : function(xhr, status, error) {
				console.log(error, status);
				console.warn(xhr.responseText);
			}
		});

		for (k in teamJson) {
			teamJson[k].COUNT_MEM = "";
			teamJson[k].COUNT_DEPT = "";

			val = JSON.parse(JSON.stringify(teamJson[k])); // deep copy

			if (pro_id_arr.indexOf(teamJson[k].TE_DEPT_KEY) != '-1') {
				template = "PmBox";
				// teamJson[k].PRO_PERCENT = "";

				ran = Math.round(Math.random() * 100) + 1; // 진척률 표시를 위한 랜덤값 생성 (1~100)
				val["PRO_PERCENT"] = ran + "%";

				for (i = 1; i <= 10; i++) {
					v = i * 10;
					if (ran > v) {
						val["PRO_" + v] = {
							"value" : " ",
							"style" : {
								"backgroundColor" : "#204161",
								"fontWeight" : "bold"
							}
						}
					} else {
						break;
					}
				}

			} else {
				template = "listTemp";
			}

			nodeData.push({
				"template" : template,
				"fields" : val
			});
		}

		// sample.json에 orgdata 넣음

		// ## getJSON 말고 json 가져오기?
		$.getJSON('./data/sample.json', function(data) {
			console.log(nodeData);
			data["orgData"] = nodeData;
			that.load(data);
		});

	},
	load : function(newNodeData) {

		viewOrg.loadJson({
			data : newNodeData
		});
	},
	click_sync : function(select_key) {
		viewOrg.nodes(select_key).select(true);
	},
	expand_sync : function(select_key, expand) {
		viewOrg.nodes(select_key).expand(expand);
	},

	button : function() {
		that = this;

		$("#zoomIn").on("click", function() {
			that.zoomIn();
		});

		$("#zoomOut").on("click", function() {
			that.zoomOut();
		});

		$("#scaleFit").on("click", function() {
			that.scaleFit();
		});

		$("#scaleFullfit").on("click", function() {
			that.scaleFullfit();
		});

		$("#saveAsImage").on("click", function() {
			that.saveAsImage();
		});

	},

	zoomIn : function() {
		viewOrg.zoomIn(true);
	},

	zoomOut : function() {
		viewOrg.zoomOut(true);
	},
	scaleFit : function() {
		viewOrg.scale("fit");
	},

	scaleFullfit : function() {
		viewOrg.scale("fullfit");
	},

	saveAsImage : function() {
		viewOrg.saveAsImage({
			"filename" : "saveImage",
			"type" : "jpg"
		});
	}

})