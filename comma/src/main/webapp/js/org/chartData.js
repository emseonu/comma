inapp.add("chartData", {
	actionMap : { // orgview 받아온 값으로 event처리
		"org_view" : {
			"click_node" : function(prop) {
				// / 반복해서 쓰일 것들은 하나의 변수에 넣어서 처리하는게 좋아요!!
				var data;
				data = prop.data;

				if (!data.binding)// binding 없으면 return
					return;

				if (data.key == "0001")// 최상위는 팝없 없음
					return;

				if (data.data.PRO_LOCA != null && data.binding == "PRO_NM") {
					this.data(data.data);
				}
			}
		}
	},
	data : function(data) {
		var set, chartD, male, female, sexdata, dist1, dist2, dist3, dist4, dist5;
		male = 0, female = 0, dist1 = 0, dist2 = 0, dist3 = 0, dist4 = 0, dist5 = 0;
		set = {
			"PRO_ID" : data.PRO_ID
		};
		$.ajax({
			type : "POST",
			url : "getChartData.do",
			data : JSON.stringify(set),
			dataType : "json",
			contentType : "application/json;charset=UTF-8",
			async : false, // json = data위함
			success : function(data) {
				chartD = data;
			},
			error : function(xhr, status, error) {
				console.log(error, status);
			}
		});

		chartD.forEach(function(d) {
			if (d.MEM_SEX == "남성") {
				male++;
			} else {
				female++;
			}
			if (0.0 < d.TE_DISTANCE && d.TE_DISTANCE <= 1.0) {
				dist1++;
			} else if (1 < d.TE_DISTANCE && d.TE_DISTANCE <= 5) {
				dist2++;
			} else if (5 < d.TE_DISTANCE && d.TE_DISTANCE <= 10) {
				dist3++;
			} else if (10 < d.TE_DISTANCE && d.TE_DISTANCE <= 15) {
				dist4++;
			} else {
				dist5++;
			}
		})

		sexdata = {

			xAxis : {
				data : [ '남성', '여성' ]
			},
			series : [ {

				data : [ {
					name : '남성',
					value : male,
					symbol : 'image://./img/man.png',
					symbolSize : [ '150', '120' ],
					label : {
						show : true,
						position : 'bottom',
						offset : [ 0, 10 ],
						fontWeight : 'bold',
						fontSize : 14,
						color : '#cc0000',
						formatter : '{c}명'
					}
				}, {
					name : '여성',
					value : female,
					symbol : 'image://./img/woman.png',
					symbolSize : [ '150', '120' ],
					label : {
						show : true,
						position : 'bottom',
						offset : [ 0, 10 ],
						fontWeight : 'bold',
						fontSize : 14,
						color : '#cc0000',
						formatter : '{c}명'
					},

				} ]

			} ]
		}

		distdata = {
			legend : {
				data : [ '통근거리' ],
			},
			yAxis : {
				name : '통근거리(km)'
			},
			series : [ {
				data : [ dist1, dist2, dist3, dist4, dist5 ],
				itemStyle : {
					color : '#61a0a8'
				},
				label : {
					show : true,
					color : '#333',
					textBorderColor : '#fff',
					textBorderWidth : 2,
					position : 'top',
					formatter : '{c}명'
				}
			} ]
		}

		inapp.raise("chartData", {
			action : "set_chart",
			data : {
				sexdata : sexdata,
				distdata : distdata

			}
		});
	}

})