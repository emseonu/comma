inapp.add("chart_view", {
	actionMap : { // orgview 받아온 값으로 event처리
		"chartData" : {
			"set_chart" : function(prop) {
				// / 반복해서 쓰일 것들은 하나의 변수에 넣어서 처리하는게 좋아요!!
				var data;

				data = prop.data;

				echarts.getInstanceByDom($("#sexChart")[0]).setOption({
					series : [ {
						data : "pictorialBar"
					} ]
				});

				this.set_data("#sexChart", data.sexdata);
				this.set_data("#distChart", data.distdata);

			}
		}
	},
	init : function() {
		var sexChart, distChart, picto_arr, bar_arr, opt_picto, opt_bar;
		picto_arr = [];
		bar_arr = [];

		// init
		sexChart = echarts.init($('#sexChart')[0]);
		distChart = echarts.init($('#distChart')[0]);
		picto_arr.push(sexChart); // pictorial chart arr
		bar_arr.push(distChart); // bar chart arr

		// pictorial chart option
		opt_picto = {
			legend : {
				data : []
			},
			tooltip : {},
			grid : {
				bottom : '90'
			},
			title : {
				text : ':',
				textStyle : {
					fontSize : 14
				},
				left : '49%',
				top : '34%',
				textAlign : 'center'
			},
			yAxis : {
				splitLine : {
					show : false
				},
				axisLabel : {
					show : false
				},
				axisTick : {
					show : false
				},
				axisLine : {
					show : false
				}
			},
			xAxis : {
				data : [],
				axisLine : {
					show : false
				},
				axisTick : {
					show : false
				},
				axisLabel : {
					margin : 45
				}
			},

			series : [ {
				type : 'pictorialBar'

			} ]
		}

		// bar chart option
		opt_bar = {
			tooltip : {},
			barWidth : '45%',
			legend : {
				icon : 'circle',
				data : [ '통근거리' ],
				bottom : 'bottom'
			},
			grid : {
				top : '30',
				width : '85%'
			},
			xAxis : {
				type : 'category',
				data : [ '~1km', '~5km', '~10km', '~15km', '15km~' ]
			},
			yAxis : {
				nameLocation : 'middle',
				nameGap : 40,
				type : 'value',
				splitNumber : 3,
				axisLine : {
					show : false
				},
				axisTick : {
					show : false
				},
			},
			series : [ {
				type : 'bar'
			} ]
		}
		console.log(picto_arr);
		this.option(picto_arr, opt_picto);
		this.option(bar_arr, opt_bar);
		console.log(bar_arr, opt_bar);
	},
	option : function(arr, option) { // 분류끼리 공통 옵션 적용
		var i, len, unit;
		len = arr.length;

		for (i = 0; i < len; i++) {
			unit = arr[i];
			unit.setOption(option);
		}
	},
	set_data : function(container, option) {
		console.log(container, option);
		echarts.init($(container)[0]).resize();
		echarts.getInstanceByDom($(container)[0]).setOption(option);

	}
})