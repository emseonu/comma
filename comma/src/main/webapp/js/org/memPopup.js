inapp.add("memPopup", {

	actionMap : { // orgview 받아온 값으로 event처리
		"org_view" : {
			"click_node" : function(prop) {
				// / 반복해서 쓰일 것들은 하나의 변수에 넣어서 처리하는게 좋아요!!
				var data;
				data = prop.data;
				// binding 없으면 return
				if (!data.binding) {
					return;
				}
				// 최상위는 팝없 없음
				if (data.key == "0001") {
					return;
				}
				// 멤버 이미지, 이름 클릭시에만 MEM POPUP
				if (data.binding == "MEM_IMG" || data.binding == "MEM_NM") {
					this.showMem(data.data);
				}
			}
		}
	},

	showMem : function(data) { // MEM POPUP
		var deptData, set, deptNm, proNm;
		set = {};
		set.DEPT_ID = data.TE_DEPT_KEY;

		$.ajax({
			type : "POST",
			url : "getProNM.do",
			data : JSON.stringify(set),
			dataType : "json",
			contentType : "application/json;charset=UTF-8",
			async : false, // json = data위함
			success : function(data) {
				console.log(data);
				deptData = data;
			},
			error : function(xhr, status, error) {
				console.log(error, status);
			}
		});

		if (deptData[0].PRO_DEPT_KEY == '0001') {
			deptNm = "manager";
			proNm = deptData[1].PRO_NM + "(" + deptData[1].PRO_DEPT_KEY + ")";
		} else {
			proNm = deptData[0].PRO_NM + "(" + deptData[0].PRO_DEPT_KEY + ")";
			deptNm = deptData[1].PRO_NM;
		}

		$('#memPopup').modal('show');
		$("#photo").attr("src", data.MEM_IMG);
		$('#mId1').html("구성원 ID");
		$('#mId1value').html(data.MEM_ID);
		$('#mId2').html("성명");
		$('#mId2value').html(data.MEM_NM);
		$('#mId3').html("참여 프로젝트");
		$('#mId3value').html(proNm);
		$('#mId4').html("부서명");
		$('#mId4value').html(deptNm);
		$('#mId5').html("거주지");
		$('#mId5value').html(data.MEM_LOCA);
	}
})
