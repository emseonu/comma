inapp.add("sheet_view", {
	cfg : {},
	init : function() {
		this.create();
		this.button();
	},
	actionMap : {

	},
	createSheet : function() {
		// sheetArea에 memSheet를 100%로 생성
		createIBSheet2(document.getElementById("sheetArea"), "memSheet", "100%", "100%");
	},
	create : function() {
		var option, that, json, sheetData, jsonData;
		option = {}, sheetData = [], jsonData = {};
		that = this;
		this.createSheet();

		option = {
			Cfg : { // 시트 기본 설정
				SearchMode : smLazyLoad, // 조회방식설정 : 스크롤 조회
				Page : 30, // 스크롤 조회에서 한번에 표시할 행의 개수
				AutoFitColWidth : "search|resize|init",
				"MergeSheet" : msHeaderOnly,
				DataRowHeight : 70

			},
			HeaderMode : { // header의 모드설정(SetHeaderMode
				// Method)
				Sort : 0,
				ColMove : 0,
				ColResize : 1, // type : Boolean, 컬럼 너비 resize
				// 여부
				HeaderCheck : 0
			},
			Cols : [ {
				Header : "구성원ID",
				Type : "Text",
				Width : 20,
				SaveName : "MEM_ID",
				Align : "Center",
				Edit : 0,
				Hidden : 0
			}, {
				Header : "상태",
				Type : "Status",
				Align : "Center",
				SaveName : "sStatus",
				Width : 15
			}, {
				Header : "삭제",
				Type : "DelCheck",
				Align : "Center",
				SaveName : "sDelCheck",
				Width : 15
			}, {
				Header : "성명",
				Type : "Text",
				Width : 20,
				SaveName : "MEM_NM",
				Align : "Center",
				Edit : 1,
				Hidden : 0
			}, {
				Header : "성별",
				Type : "Combo",
				Width : 15,
				SaveName : "MEM_SEX",
				Align : "Center",
				ComboText : "남성|여성",
				ComboCode : "남성|여성",
				Edit : 1,
				Hidden : 0,
			}, {
				Header : "이미지",
				Type : "Image",
				Width : 10,
				MaxWidth : 10,
				ImgWidth : 67,
				SaveName : "MEM_IMG",
				Align : "Center",
				Edit : 1,
				Hidden : 0
			}, {
				Header : "이미지",
				Type : "Button",
				Align : "Center",
				SaveName : "sButton",
				Width : 10
			}, {
				Header : "이미지첨부",
				Type : "text",
				Align : "Center",
				SaveName : "MEM_NEW_IMAGE",
				Width : 20,
				Hidden : 1
			}, {
				Header : "거주지",
				Type : "Text",
				Width : 60,
				SaveName : "MEM_LOCA",
				Edit : 1,
				Hidden : 0
			}, {
				Header : "MEM_LOCA_Y",
				Type : "Text",
				Width : 60,
				SaveName : "MEM_LOCA_Y",
				Edit : 0,
				Hidden : 1
			}, {
				Header : "MEM_LOCA_X",
				Type : "Text",
				Width : 60,
				SaveName : "MEM_LOCA_X",
				Edit : 0,
				Hidden : 1

			} ]
		}
		IBS_InitSheet(memSheet, option);
		memSheet.SetTreeActionMode(1); // 삭제 선택 시 자식 행까지 모두 삭제
		memSheet.ShowFilterRow(); // filter
		memSheet.SetRowHidden(1, 1);// filter hidden
		memSheet.SetWaitImageVisible(0);

		// 구성원관리 데이터 : setMemData
		$.ajax({
			type : "POST",
			url : "getMemData.do",
			dataType : "json",
			contentType : "application/json;charset=UTF-8",
			async : false, // json = data위함
			success : function(data) {
				console.log(data);
				json = data;
			},
			error : function(xhr, status, error) {
				console.log(error, status);
			}
		});

		for (k in json) {
			json[k].sButton = "<img src='img/photo.png',width='14', height='14'>";
		}

		jsonData["data"] = json;
		this.load(jsonData);
		this.bind(); // event

	},

	button : function() {
		var row, col, s, img, imgData, dropdown, searchText;
		that = this;
		s = memSheet;

		// 검색
		$("#searchMem").click(function() {
			dropdown = $("#dropdownMem").val();
			searchText = $("#sTextMem").val();
			that.search(dropdown, searchText);
		});

		// 검색 reset
		$("#dropdownMem").on("change", function() {
			$("#sTextMem").val("");
			memSheet.ClearFilterRow();
			memSheet.SetRowHidden(1, 1);
		});

		// 구성원 추가 버튼
		$("#insertMem").click(function() {
			that.insertMem();
		});

		// 저장 버튼
		$("#save").click(function() {
			that.loca_save();
		});

		// 이미지 첨부 클릭시 file 클릭
		$("#file").on("change", function() {
			formData = new FormData();
			row = s.GetSelectRow();
			id = s.GetCellValue(row, 0);
			img = $('#file')[0].files[0];
			formData.set(id, img); // 덮어쓰기
			s.SetCellValue(row, 7, img.name);

			// 이미지 저장: savememSheet
			$.ajax({
				type : "POST",
				url : "saveMemImg.do",
				data : formData,
				enctype : "multipart/form-data",
				processData : false,
				contentType : false,
				async : false, // json = data위함
				success : function(data) {
					s.SetCellValue(row, 5, data); // 이미지 변경
				},
				error : function(xhr, status, error) {
					console.log(error, status);
					console.warn(xhr.responseText);
				}
			});
		});

	},
	search : function(dropdown, searchText) {
		var col;
		if (searchText == "") {
			memSheet.ClearFilterRow();
			memSheet.SetRowHidden(1, 1);
			return;
		}

		if (dropdown == "성명") {
			col = "MEM_NM";
		} else if (dropdown == "구성원ID") {
			col = "MEM_ID";
		} else if (dropdown == "거주지") {
			col = "MEM_LOCA";
		}
		if (searchText == "") {
			memSheet.ClearFilterRow();
			return;
		}

		memSheet.SetFilterValue(col, searchText, 11);

	},
	insertMem : function() {
		var row, s;
		s = memSheet;// sheet id
		s.DataInsert();
		row = s.GetSelectRow(); // 해당 행에 시작, 종료, 위치 입력 가능

		$.post("getMemNextID.do", function(data) { // database mem.sequence 미리 가져와서 출력
			s.SetCellValue(row, 0, data);
		});
		s.SetCellValue(row, 6, "<img src='img/photo.png',width='14', height='14'>");
	},
	loca_save : function() {
		var that, s, saveJson, geocoder, counter, counterN;
		that = this, s = memSheet, counter = 0, counterN = 0;

		geocoder = new kakao.maps.services.Geocoder(); // 주소-좌표 변환 객체
		saveJson = s.GetSaveJson();

		if (saveJson.data.length <= 0) {
			if (saveJson.Message == "NoTargetRows") {
				alert("저장할 내역이 없습니다.");
				return;
			}
		}

		saveJson.data.forEach(function(d) { // 주소-좌표 변환
			geocoder.addressSearch(d.MEM_LOCA, function(result, status) {
				if (status === kakao.maps.services.Status.OK) {// 정상적으로 검색이 완료
					d.MEM_LOCA_Y = result[0].y;
					d.MEM_LOCA_X = result[0].x;
				} else {// 주소 검색 오류
					alert(d.MEM_NM + "님의 정확한 거주지를 입력해 주세요.");
					return;
				}
				counter++;
				if (counter == saveJson.data.length) {
					that.setAjax(saveJson);
				}
			});

		});
	},
	setAjax : function(saveJson) {// loca-save 성공시 data 저장
		var that, s, rst;
		that = this, s = memSheet, counter = 0, counterN = 0;
		var data = JSON.stringify(saveJson);

		$.ajax({// member sheet 저장
			type : "POST",
			url : "saveMemSheet.do",
			data : data,
			dataType : "json",
			contentType : "application/json;charset=UTF-8",
			success : function(data) {
				rst = {
					"Result" : {
						"Code" : 0,
						"Message" : "저장 되었습니다."
					}
				};
				s.LoadSaveData(rst);

			},
			error : function(xhr, status, error) {
				console.log(error, status);
				console.warn(xhr.responseText);
			}
		});
	},
	bind : function() {
		var that, upId;
		that = this;

		function addEvents(memSheet, callbacks) {
			for ( var prop in callbacks) {
				window[memSheet + "_" + prop] = callbacks[prop];
			}
		}
		addEvents(memSheet.id, {
			OnButtonClick : function(row, col) { // image button 클릭 시 파일 업로드 클릭
				$('#file').click();
			},
			OnSaveEnd : function(code) {
				if (code == 0) {
					alert("저장에 성공하였습니다.");
				} else {
					alert("저장에 실패하였습니다.");
				}
			}
		});
	},

	load : function(jsonData) {
		memSheet.LoadSearchData(jsonData);
	}
})